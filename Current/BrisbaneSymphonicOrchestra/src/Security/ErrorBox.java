package Security;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class ErrorBox {
	
	/**
	 * Creates an error box to display to the user
	 * @param frame - frame to add to
	 * @param errorMsg - the message to display to the user
	 * @param errorMsgTitle - the title of the message box 
	 * @param type - an integer specifying the kind of message to display: 
	 * ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE, QUESTION_MESSAGE, or PLAIN_MESSAGE
	 */
	public static void createErrorBox(JFrame frame, String errorMsg, String errorMsgTitle, int type) {
		JOptionPane pane = new JOptionPane();
		JDialog dialog = pane.createDialog(frame, errorMsgTitle);
		pane.setMessageType(type);
		pane.setMessage(errorMsg);
		dialog.setSize(310, 120);
		dialog.setLocationRelativeTo(frame);
		dialog.setVisible(true);
	}
	
	/**
	 * Creates a confirmation box that returns the option chosen by the user
	 * @param frame - frame to add to
	 * @param errorMsg - the message to display to the user
	 * @param errorMsgTitle - the title of the message box 
	 * @return - an int of the user's selection (0 or 1)
	 */
	public static int confirmBox(JFrame frame, String errorMsg, String errorMsgTitle) {
		return JOptionPane.showConfirmDialog(frame, errorMsg, errorMsgTitle, JOptionPane.YES_NO_OPTION, 
				JOptionPane.QUESTION_MESSAGE);
	}
}
