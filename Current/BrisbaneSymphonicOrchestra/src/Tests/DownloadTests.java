package Tests;

import static org.junit.Assert.*;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.BeforeClass;
import org.junit.Test;

import DatabaseConn.Download;

public class DownloadTests {
	private static Path currentRelativePath = Paths.get("");
	private static String currentRelativePathString = currentRelativePath.toAbsolutePath().toString();
	private static Download dl;
	
	@BeforeClass 
	public static void setup() {
		dl = new Download(currentRelativePathString + "/test.pdf","http://localhost/Project/example.pdf");
	}

	@Test
	public void downloadPDF() {
		assertTrue(new File(currentRelativePathString + "/test.pdf").exists());
	}
	
	@Test
	public void createBlankPDF() {
		dl.createPDF(currentRelativePathString + "/test2.pdf");
		assertTrue(new File(currentRelativePathString + "/test2.pdf").exists());
	}
}
