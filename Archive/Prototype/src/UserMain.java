import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.List;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class UserMain {

	protected Shell shlBrisbaneSymphonicBand;
	private Text text;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			UserMain window = new UserMain();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shlBrisbaneSymphonicBand.open();
		shlBrisbaneSymphonicBand.layout();
		while (!shlBrisbaneSymphonicBand.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlBrisbaneSymphonicBand = new Shell();
		shlBrisbaneSymphonicBand.setSize(450, 300);
		shlBrisbaneSymphonicBand.setText("Brisbane Symphonic Band");
		
		Button button1 = new Button(shlBrisbaneSymphonicBand, SWT.NONE);
		button1.setBounds(334, 10, 90, 25);
		button1.setText("Search");
		
		List list = new List(shlBrisbaneSymphonicBand, SWT.BORDER | SWT.V_SCROLL);
		list.setItems(new String[] {"Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6", "Item 7", "Item 8", "Item 9", "Item 10", "Item 11", "Item 12", "Item 13", "Item 14", "Item 15"});
		list.setBackground(SWTResourceManager.getColor(255, 255, 153));
		list.setBounds(0, 79, 434, 182);
		
		Button button2 = new Button(shlBrisbaneSymphonicBand, SWT.NONE);
		button2.setText("Logout");
		button2.setBounds(334, 48, 90, 25);
		
		text = new Text(shlBrisbaneSymphonicBand, SWT.BORDER);
		text.setBounds(233, 10, 96, 25);
		
		Label lblLoggedInAs = new Label(shlBrisbaneSymphonicBand, SWT.NONE);
		lblLoggedInAs.setBounds(0, 0, 227, 25);
		lblLoggedInAs.setText("Logged in as: UserName");

	}
}