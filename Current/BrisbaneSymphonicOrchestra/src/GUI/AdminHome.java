package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.Window;

import javax.swing.JTabbedPane;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import java.awt.Font;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import org.apache.commons.codec.binary.Hex;

import DatabaseConn.Connect;
import Security.ErrorBox;
import Security.Hash;

import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyVetoException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.awt.event.ActionEvent;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;

import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;

import java.awt.Color;
import javax.swing.JInternalFrame;
import javax.swing.JDesktopPane;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import javax.swing.JToggleButton;



public class AdminHome {	
	private JFrame frame;
	private JTabbedPane tabbedPane;
	private JTextField txtUsername;
	private JTextField txtFirstname;
	private JTextField txtLastname;
	private JPasswordField pswPassword;
	private JPasswordField pswPassCheck;
	private JTextField txtEmail;
	private JComboBox cmbUserType;
	private JTextField txtPieceID;
	private JTextField txtTitle;
	private JTextField txtPublisher;
	private JTextField txtSx;
	private JTextField txtXmas;
	private JTextField txtMin;
	private JTextField txtComment;
	private JTextField txtSec;
	private JTextField txtMusicType;
	private JTextField txtEthnic;
	private JTextField txtArranger;
	private JTextField txtComposer;
	private JTextField txtExists;
	private JTextField txtRhythm;
	private JTextField txtCw;
	private JTextField txtInstrID;
	private JTextField txtInstrName;
	private JComboBox cmbInstrBand;
	private JTextField txtPartID;
	private JTextField txtPartName;
	private JTextField txtPartPieceID;
	private JTextField txtPartLoc;
	private JTextField txtPartInstrID;
	private JComboBox cmbStartDateDay;
	private JComboBox cmbStartDateMonth;
	private JComboBox cmbStartDateYear;
	private JComboBox cmbEndDateDay;
	private JComboBox cmbEndDateMonth;
	private JComboBox cmbEndDateYear;
	
	Path currentRelativePath = Paths.get("");
	String currentRelativePathString = currentRelativePath.toAbsolutePath().toString();
	ImageIcon displayImage = new ImageIcon(
			currentRelativePathString + "\\icons\\DisplayIcon.png");
	ImageIcon editImage = new ImageIcon(
			currentRelativePathString + "\\icons\\EditIcon.png");
	ImageIcon deleteImage = new ImageIcon(
			currentRelativePathString + "\\icons\\DeleteIcon.png");
	ImageIcon logoutImage = new ImageIcon(
			currentRelativePathString + "\\icons\\LogoutIcon.png");
	ImageIcon selectImage = new ImageIcon(
			currentRelativePathString + "\\icons\\SelectIcon.png");
	ImageIcon addImage = new ImageIcon(
			currentRelativePathString + "\\icons\\AddIcon.png");
	
	
	
	
	JTextField[] txtPieces;
	JTextField[] txtUsers;
	JTextField[] txtLicenses;
	JTextField[] txtParts;
	JTextField[] txtInstruments;
	JTextField[][] txtSets;
	public HashMap <JButton, JTextField> searchHash;
	public HashMap <JButton, JTable> searchTables;
	
	//HashMap<JTable, String> allTables;
	
	private Connect connection;
	private Object[][] pieceData;
	private Object[][] userData;
	private Object[][] licenseData;
	private Object[][] partData;
	private Object[][] instrumentData;
	private DefaultTableModel modelPieces;
	private DefaultTableModel modelUsers;
	private DefaultTableModel modelLicenses;
	private DefaultTableModel modelParts;
	private DefaultTableModel modelInstruments;
	private Object partIDSelected = "";
	private Object userNameSelected = "";
	private Object pieceSelected = "";
	private Object instrumentSelected = "";
	
	private int NUM_OF_INT_FRAMES = 4;
	private int NUM_OF_DATA_TABS = 5;
	//Account for zero, 3 is the fourth position
	private int SEPARATOR_TAB_POS = 3;
	
	/*
	 * Order of "int" (internal frame) items is: pieces, instruments, parts, users
	 * Order of "main" items is: pieces,pieceparts,licenses,bandusers,instruments
	 * Order of items common to both is "main" order followed by "int" order
	 */
	private JPanel[] mainPanelsMain = new JPanel[NUM_OF_DATA_TABS]; 
	private JPanel[] mainPanelsLeft = new JPanel[NUM_OF_DATA_TABS];
	private JPanel[] mainPanelsRight = new JPanel[NUM_OF_DATA_TABS];
	private JButton[] addButtons = new JButton[NUM_OF_DATA_TABS];
	private JButton[] editButtons = new JButton[NUM_OF_DATA_TABS];
	private JButton[] deleteButtons = new JButton[NUM_OF_DATA_TABS];
	private JButton[] clearPrefillButtons = new JButton[NUM_OF_DATA_TABS + NUM_OF_INT_FRAMES];
	private JButton btnPieceNotSelected;
	private JButton btnUserNotSelected;
	private JButton btnInstrumentNotSelected;
	private JButton btnPartNotSelected;
	
	private JPanel[] bothPanelsBot = new JPanel[NUM_OF_DATA_TABS + NUM_OF_INT_FRAMES];
	private JPanel[] panelBotLeft = new JPanel[NUM_OF_DATA_TABS + NUM_OF_INT_FRAMES];
	private JTable[] jTables = new JTable[NUM_OF_DATA_TABS + NUM_OF_INT_FRAMES];
	private JScrollPane[] scrollPanes = new JScrollPane[NUM_OF_DATA_TABS + NUM_OF_INT_FRAMES];
	private JButton[] searchButtons = new JButton[NUM_OF_DATA_TABS + NUM_OF_INT_FRAMES];
	private JButton[] refreshButtons = new JButton[NUM_OF_DATA_TABS + NUM_OF_INT_FRAMES];
	private JTextField[] searchTextFields = new JTextField[NUM_OF_DATA_TABS + NUM_OF_INT_FRAMES];
	private JComboBox[] searchComboBoxes = new JComboBox[NUM_OF_DATA_TABS + NUM_OF_INT_FRAMES];
	
	
	private String[] properTableNames = new String[] {"PIECES", "PARTS", "LICENSES", "USERS", "INSTRUMENTS", "PIECES", "IINSTRUMENTS", "PARTS", "USERS"};
	private String[] trueTableNames = new String[] {"pieces","pieceparts","licenses","bandusers","instruments","pieces","instruments","pieceparts","bandusers"};
	private String[][] columnNames = new String [][] {{"Piece ID", "Piece Exists", "Title", "Arranger", "Composer", "Publisher", "XMAS", "Sx", "Min", "Sec", "Comment", "CW", "Music_type", "Ethnic", "Rhythm"},
														{"Part ID", "Part Name", "Piece ID", "Part Location", "Instrument ID"},
														{"License ID", "Username", "Part ID", "Start License Date", "End License Date"},
														{"UserName", "Email", "Last Name", "First Name", "User Type"},
														{"Instrument ID", "Instrument Name", "Instrument Band"},
														{"Piece ID", "Piece Exists", "Title", "Arranger", "Composer", "Publisher", "XMAS", "Sx", "Min", "Sec", "Comment", "CW", "Music_type", "Ethnic", "Rhythm"},
														{"Instrument ID", "Instrument Name", "Instrument Band"},
														{"Part ID", "Part Name", "Piece ID", "Part Location", "Instrument ID"},
														{"UserName", "Email", "Last Name", "First Name", "User Type"},};
	
	private int WINDOW_X = 100;
	private int WINDOW_Y = 100;
	private int WINDOW_LEN = 1280;
	private int WINDOW_HGT = 720;
	
	private int INTFRAME_LEN = 1060;
	private int INTFRAME_HGT = 510;
	
	/**
	 * Create the application.
	 */
	public AdminHome(Connect passedConnection) {
		connection = passedConnection;
		initialize();
	}
	
	
	/**
	 * Adds panels to the frame by number
	 * @param i - The Number used to identify the 'table'
	 */
	private void AddMainPanels(int i) {
		mainPanelsMain[i] = new JPanel();
		mainPanelsMain[i].setLayout(null);
		mainPanelsMain[i].setBackground(new Color(13, 51, 92));
		tabbedPane.addTab(properTableNames[i], null, mainPanelsMain[i], null);
		JLabel lab = new JLabel(properTableNames[i]);
		lab.setPreferredSize(new Dimension(205, 40));
		lab.setFont(new Font(Font.SERIF, Font.BOLD, 17));
		lab.setBackground(new Color(255, 255, 255));
		tabbedPane.setTabComponentAt(i, lab);
		tabbedPane.setBackground(new Color(255, 255, 255));

		mainPanelsLeft[i] = new JPanel();
		mainPanelsLeft[i].setLayout(null);
		mainPanelsLeft[i].setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		mainPanelsLeft[i].setBounds(10, 11, 520, 550);
		mainPanelsLeft[i].setBackground(new Color(13, 51, 92));
		mainPanelsMain[i].add(mainPanelsLeft[i]);
		
		mainPanelsRight[i] = new JPanel();
		mainPanelsRight[i].setLayout(null);
		mainPanelsRight[i].setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		mainPanelsRight[i].setBounds(535, 11, 729, 550);
		mainPanelsRight[i].setBackground(new Color(13, 51, 92));
		mainPanelsMain[i].add(mainPanelsRight[i]);
		
		jTables[i] = new JTable();
		jTables[i].setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		jTables[i].setModel(new DefaultTableModel(new Object[][] {}, columnNames[i]));
		jTables[i].setFillsViewportHeight(true);
		jTables[i].setAutoCreateRowSorter(true);
		jTables[i].getTableHeader().setReorderingAllowed(false);


		// Add pieces table to scroll pane
		scrollPanes[i] = new JScrollPane(jTables[i]);
		scrollPanes[i].setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPanes[i].setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPanes[i].setBounds(10, 10, 709, 525);
		mainPanelsRight[i].add(scrollPanes[i]);

		bothPanelsBot[i] = new JPanel();
		bothPanelsBot[i].setBounds(450, 576, 800, 38);
		bothPanelsBot[i].setBackground(new Color(13, 51, 92));
		mainPanelsMain[i].add(bothPanelsBot[i]);
		
		panelBotLeft[i] = new JPanel();
		panelBotLeft[i].setBounds(40, 576, 400, 38);
		panelBotLeft[i].setBackground(new Color(13, 51, 92));
		mainPanelsMain[i].add(panelBotLeft[i]);
		// Event Listener for tables
		// Used to get values from the selected row
		jTables[i].getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent event) {
				fillTextBoxes(i);
			}
		});		
		
		addButtons[i] = new JButton("Add", addImage);
		panelBotLeft[i].add(addButtons[i]);
		addButtons[i].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Checks(i, "add");
			}
		});
		
		editButtons[i] = new JButton("Save", editImage);
		panelBotLeft[i].add(editButtons[i]);
		editButtons[i].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (jTables[i].getSelectionModel().isSelectionEmpty()) {
					ErrorBox.createErrorBox(frame, "Please select a row", "Error", 0);
				}
				
				Checks(i, "edit");
			}
		});
		
		deleteButtons[i] = new JButton("Delete", deleteImage);
		bothPanelsBot[i].add(deleteButtons[i]);
		deleteButtons[i].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteRow(jTables[i], trueTableNames[i]);
			}
		});	
		

		clearPrefillButtons[i] = new JButton("Clear Text");
		clearPrefillButtons[i].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				jTables[i].clearSelection();
				switch(trueTableNames[i])  {
					case "pieces": 
						clearTextBoxes(txtPieces);
						break;
					case "pieceparts":
						clearTextBoxes(txtParts);
						btnPieceNotSelected.setText("Piece: Not Selected");
						btnInstrumentNotSelected.setText("Instrument: Not Selected");
						pieceSelected = "";
						instrumentSelected = "";
					case "licenses":
						btnPartNotSelected.setText("Part: Not Selected");
						btnUserNotSelected.setText("User: Not Selected");
						cmbEndDateYear.setSelectedItem("2000");
						cmbEndDateMonth.setSelectedItem("01");
						cmbEndDateDay.setSelectedItem("01");
						cmbStartDateYear.setSelectedItem("2000");
						cmbStartDateMonth.setSelectedItem("01");
						cmbStartDateDay.setSelectedItem("01");
						userNameSelected = "";
						partIDSelected = "";
					case "bandusers":
						clearTextBoxes(txtUsers);
						cmbUserType.setSelectedItem("User");
					case "instruments":
						clearTextBoxes(txtInstruments);
						
				}
			}
		});
		panelBotLeft[i].add(clearPrefillButtons[i]);
	}
	
	
	/**
	 * Adds search elements to the frame by number
	 * @param i - The Number used to identify the 'table'
	 */
	private void PutSearchSet(int i) {
		//Initialize the search term textfield, the search button and the refresh button with their actionlisteners
		searchTextFields[i] = new JTextField("", 10);
		
		searchComboBoxes[i] = new JComboBox(connection.returnColumnNames(trueTableNames[i]));
		
		searchButtons[i] = new JButton("Search");
		searchButtons[i].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				
				Object[][] data = connection.returnFilteredData(trueTableNames[i], searchTextFields[i].getText(), (String) searchComboBoxes[i].getSelectedItem());
				jTables[i].setModel(new DefaultTableModel(data, columnNames[i]));
			}
		});
		
		refreshButtons[i] = new JButton("Clear Search");
		refreshButtons[i].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				jTables[i].setModel(new DefaultTableModel(connection.returnDBData(trueTableNames[i]), columnNames[i]) {
					public boolean isCellEditable(int row, int column) {
						return false;
					}
				});
			}
		});
				
		
		//Add the search items to their respective panels
		bothPanelsBot[i].add(searchTextFields[i]);
		bothPanelsBot[i].add(searchComboBoxes[i]);
		bothPanelsBot[i].add(searchButtons[i]);
		bothPanelsBot[i].add(refreshButtons[i]);
		//bothPanelsBot[i].add(clearPrefillButtons[i]);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Brisbane Symphonic Band");
		frame.setBounds(WINDOW_X, WINDOW_Y, WINDOW_LEN, WINDOW_HGT);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);		
		
		JButton btnLogout = new JButton("Logout", logoutImage);
		btnLogout.setFont(new Font(Font.SERIF, Font.BOLD, 17));
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure?","question",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
					frame.dispose();
					connection.closeConnection();
					LoginPage login = new LoginPage();
				}			
			}
		});
		btnLogout.setBounds(1133, 2, 140, 44);
		frame.getContentPane().add(btnLogout);
		
		//Add Internal Frames
		JInternalFrame[] internalFrames = new JInternalFrame[NUM_OF_INT_FRAMES];
		JPanel[] displayPanels = new JPanel[NUM_OF_INT_FRAMES];
		JButton[] intSelectButtons = new JButton[NUM_OF_INT_FRAMES];
		
		for (int i = 0; i < NUM_OF_INT_FRAMES; i++) {
			internalFrames[i] = new JInternalFrame(properTableNames[i + NUM_OF_DATA_TABS]);
			internalFrames[i].setClosable(true);
			frame.getContentPane().add(internalFrames[i]);
			internalFrames[i].setBounds((WINDOW_LEN - INTFRAME_LEN)/2, ((WINDOW_HGT - INTFRAME_HGT)/2) + 90 , INTFRAME_LEN, INTFRAME_HGT);
			internalFrames[i].setBackground(new Color(13, 51, 92));
			internalFrames[i].getContentPane().setLayout(null);
			
			displayPanels[i] = new JPanel();
			displayPanels[i].setBounds(80, 46, 890, 300);
			displayPanels[i].setBackground(new Color(13, 51, 92));
			internalFrames[i].getContentPane().add(displayPanels[i]);
			displayPanels[i].setLayout(null);
			
			jTables[i + NUM_OF_DATA_TABS] = new JTable();
			jTables[i + NUM_OF_DATA_TABS].setModel(new DefaultTableModel(new Object[][] {}, columnNames[i + NUM_OF_DATA_TABS]));
			jTables[i + NUM_OF_DATA_TABS].setFillsViewportHeight(true);
			jTables[i + NUM_OF_DATA_TABS].setEnabled(false);
			jTables[i + NUM_OF_DATA_TABS].setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

			scrollPanes[i + NUM_OF_DATA_TABS] = new JScrollPane(jTables[i + NUM_OF_DATA_TABS]);
			scrollPanes[i + NUM_OF_DATA_TABS].setBounds(0, 0, 890, 300);
			displayPanels[i].add(scrollPanes[i + NUM_OF_DATA_TABS]);
			scrollPanes[i + NUM_OF_DATA_TABS].setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			scrollPanes[i + NUM_OF_DATA_TABS].setEnabled(false);
			scrollPanes[i + NUM_OF_DATA_TABS].setVisible(false);
			scrollPanes[i + NUM_OF_DATA_TABS].setViewportView(jTables[i + NUM_OF_DATA_TABS]);
			
			bothPanelsBot[i + NUM_OF_DATA_TABS] = new JPanel();
			bothPanelsBot[i + NUM_OF_DATA_TABS].setBounds(210, 358, 700, 38);
			bothPanelsBot[i + NUM_OF_DATA_TABS].setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			bothPanelsBot[i + NUM_OF_DATA_TABS].setBackground(new Color(13, 51, 92));
			internalFrames[i].getContentPane().add(bothPanelsBot[i + NUM_OF_DATA_TABS]);
			
			panelBotLeft[i + NUM_OF_DATA_TABS] = new JPanel();
			panelBotLeft[i + NUM_OF_DATA_TABS].setBounds(210, 358, 700, 38);
			panelBotLeft[i + NUM_OF_DATA_TABS].setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			panelBotLeft[i + NUM_OF_DATA_TABS].setBackground(new Color(13, 51, 92));
			internalFrames[i].getContentPane().add(panelBotLeft[i + NUM_OF_DATA_TABS]);
			
			intSelectButtons[i] = new JButton("Select", selectImage);
			intSelectButtons[i].setBounds(250, 150, 30, 50);
			bothPanelsBot[i + NUM_OF_DATA_TABS].add(intSelectButtons[i]);
			
			internalFrames[i].addInternalFrameListener(new InternalFrameAdapter()
	        {
	            @Override
	            public void internalFrameClosing(InternalFrameEvent e)
	            {
	                setEnabledRecursive(tabbedPane, true);
	                e.getInternalFrame().dispose();
	            }
	        });
			
		}
		
		
		/*
		 * Main Tabbed Pane
		 */

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 1274, 690);
		frame.getContentPane().add(tabbedPane);		
		
		//Add the tabs before the separator
		for (int i = 0; i < SEPARATOR_TAB_POS; i++) {
			AddMainPanels(i);
		}
		
		//Add the "Separator" tab
		JPanel pnlTabSeperator = new JPanel();
		
		//Add the tabs after the separator
		for (int i = SEPARATOR_TAB_POS; i < NUM_OF_DATA_TABS; i++) {
			AddMainPanels(i);
		}
		
		//By this point, all the tables exist, the search can be added on the end of both kinds
		for (int i = 0; i < searchButtons.length; i++) {
			
			PutSearchSet(i);
		}
		
		
		//			*Pieces

		JLabel lblExists = new JLabel("Exists:");
		lblExists.setHorizontalAlignment(SwingConstants.LEFT);
		lblExists.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblExists.setBounds(347, 260, 60, 25);
		lblExists.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[0].add(lblExists);

		JLabel lblTitle = new JLabel("Title:");
		lblTitle.setHorizontalAlignment(SwingConstants.LEFT);
		lblTitle.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblTitle.setBounds(10, 20, 125, 25);
		lblTitle.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[0].add(lblTitle);

		JLabel lblArranger = new JLabel("Arranger:");
		lblArranger.setHorizontalAlignment(SwingConstants.LEFT);
		lblArranger.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblArranger.setBounds(10, 80, 125, 25);
		lblArranger.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[0].add(lblArranger);

		JLabel lblComposer = new JLabel("Composer:");
		lblComposer.setHorizontalAlignment(SwingConstants.LEFT);
		lblComposer.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblComposer.setBounds(10, 140, 125, 25);
		lblComposer.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[0].add(lblComposer);

		JLabel lblPublisher = new JLabel("Publisher:");
		lblPublisher.setHorizontalAlignment(SwingConstants.LEFT);
		lblPublisher.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblPublisher.setBounds(10, 200, 125, 25);
		lblPublisher.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[0].add(lblPublisher);

		JLabel lblXmas = new JLabel("XMAS:");
		lblXmas.setHorizontalAlignment(SwingConstants.LEFT);
		lblXmas.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblXmas.setBounds(10, 260, 125, 25);
		lblXmas.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[0].add(lblXmas);

		JLabel lblSx = new JLabel("Sx:");
		lblSx.setHorizontalAlignment(SwingConstants.LEFT);
		lblSx.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblSx.setBounds(192, 260, 60, 25);
		lblSx.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[0].add(lblSx);

		JLabel lblTime = new JLabel("Time:");
		lblTime.setHorizontalAlignment(SwingConstants.LEFT);
		lblTime.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblTime.setBounds(10, 320, 125, 25);
		lblTime.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[0].add(lblTime);

		JLabel lblMin = new JLabel("Min");
		lblMin.setHorizontalAlignment(SwingConstants.LEFT);
		lblMin.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblMin.setBounds(192, 320, 55, 25);
		lblMin.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[0].add(lblMin);

		JLabel lblSec = new JLabel("Sec");
		lblSec.setHorizontalAlignment(SwingConstants.LEFT);
		lblSec.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblSec.setBounds(340, 320, 55, 25);
		lblSec.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[0].add(lblSec);

		JLabel lblComment = new JLabel("Comment:");
		lblComment.setHorizontalAlignment(SwingConstants.LEFT);
		lblComment.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblComment.setBounds(10, 380, 125, 25);
		lblComment.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[0].add(lblComment);

		JLabel lblCw = new JLabel("CW:");
		lblCw.setHorizontalAlignment(SwingConstants.LEFT);
		lblCw.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblCw.setBounds(305, 440, 45, 25);
		lblCw.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[0].add(lblCw);

		JLabel lblMusicType = new JLabel("Music Type:");
		lblMusicType.setHorizontalAlignment(SwingConstants.LEFT);
		lblMusicType.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblMusicType.setBounds(10, 440, 104, 25);
		lblMusicType.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[0].add(lblMusicType);

		JLabel lblEthnic = new JLabel("Ethnic:");
		lblEthnic.setHorizontalAlignment(SwingConstants.LEFT);
		lblEthnic.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblEthnic.setBounds(10, 500, 87, 25);
		lblEthnic.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[0].add(lblEthnic);

		JLabel lblRhythm = new JLabel("Rhythm:");
		lblRhythm.setHorizontalAlignment(SwingConstants.LEFT);
		lblRhythm.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblRhythm.setBounds(272, 500, 87, 25);
		lblRhythm.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[0].add(lblRhythm);

		txtExists = new JTextField();
		txtExists.setColumns(10);
		txtExists.setBounds(417, 260, 87, 25);
		mainPanelsLeft[0].add(txtExists);

		txtTitle = new JTextField();
		txtTitle.setColumns(10);
		txtTitle.setBounds(124, 20, 361, 25);
		mainPanelsLeft[0].add(txtTitle);

		txtArranger = new JTextField();
		txtArranger.setBounds(124, 80, 361, 25);
		mainPanelsLeft[0].add(txtArranger);
		txtArranger.setColumns(10);

		txtComposer = new JTextField();
		txtComposer.setColumns(10);
		txtComposer.setBounds(124, 140, 361, 25);
		mainPanelsLeft[0].add(txtComposer);

		txtPublisher = new JTextField();
		txtPublisher.setColumns(10);
		txtPublisher.setBounds(124, 200, 361, 25);
		mainPanelsLeft[0].add(txtPublisher);

		txtXmas = new JTextField();
		txtXmas.setColumns(10);
		txtXmas.setBounds(75, 260, 87, 25);
		mainPanelsLeft[0].add(txtXmas);

		txtSx = new JTextField();
		txtSx.setColumns(10);
		txtSx.setBounds(248, 260, 87, 25);
		mainPanelsLeft[0].add(txtSx);

		txtMin = new JTextField();
		txtMin.setColumns(10);
		txtMin.setBounds(124, 320, 66, 25);
		mainPanelsLeft[0].add(txtMin);

		txtSec = new JTextField();
		txtSec.setColumns(10);
		txtSec.setBounds(272, 320, 66, 25);
		mainPanelsLeft[0].add(txtSec);

		txtComment = new JTextField();
		txtComment.setColumns(10);
		txtComment.setBounds(124, 380, 361, 25);
		mainPanelsLeft[0].add(txtComment);

		txtCw = new JTextField();
		txtCw.setColumns(10);
		txtCw.setBounds(347, 440, 125, 25);
		mainPanelsLeft[0].add(txtCw);

		txtMusicType = new JTextField();
		txtMusicType.setColumns(10);
		txtMusicType.setBounds(124, 440, 125, 25);
		mainPanelsLeft[0].add(txtMusicType);

		txtEthnic = new JTextField();
		txtEthnic.setColumns(10);
		txtEthnic.setBounds(124, 500, 125, 25);
		mainPanelsLeft[0].add(txtEthnic);

		txtRhythm = new JTextField();
		txtRhythm.setColumns(10);
		txtRhythm.setBounds(347, 500, 125, 25);
		mainPanelsLeft[0].add(txtRhythm);

		
		
		
		// 			*Parts
		
		btnPieceNotSelected = new JButton("Piece: Not Selected");
		btnPieceNotSelected.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				internalFrames[0].setEnabled(true);
				internalFrames[0].setVisible(true);
				bothPanelsBot[NUM_OF_DATA_TABS].setVisible(true);
				
				scrollPanes[NUM_OF_DATA_TABS].setEnabled(true);
				scrollPanes[NUM_OF_DATA_TABS].setVisible(true);
				jTables[NUM_OF_DATA_TABS].setEnabled(true);
				jTables[NUM_OF_DATA_TABS].setVisible(true);
				setEnabledRecursive(tabbedPane, false);
			}
		});
		btnPieceNotSelected.setBounds(200, 20, 200, 25);
		mainPanelsLeft[1].add(btnPieceNotSelected);
		
		btnInstrumentNotSelected = new JButton("Instrument: Not Selected");
		btnInstrumentNotSelected.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				internalFrames[1].setEnabled(true);
				internalFrames[1].setVisible(true);
				bothPanelsBot[1 + NUM_OF_DATA_TABS].setVisible(true);
				scrollPanes[1 + NUM_OF_DATA_TABS].setEnabled(true);
				scrollPanes[1 + NUM_OF_DATA_TABS].setVisible(true);
				jTables[1 + NUM_OF_DATA_TABS].setEnabled(true);
				jTables[1 + NUM_OF_DATA_TABS].setVisible(true);
				setEnabledRecursive(tabbedPane, false);
			}
		});
		btnInstrumentNotSelected.setBounds(200, 100, 200, 25);
		mainPanelsLeft[1].add(btnInstrumentNotSelected);
		
		JLabel lblSelectPiece = new JLabel("Select Piece:");
		lblSelectPiece.setHorizontalAlignment(SwingConstants.LEFT);
		lblSelectPiece.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblSelectPiece.setBounds(10, 20, 125, 25);
		lblSelectPiece.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[1].add(lblSelectPiece);

		JLabel lblSelectInstr = new JLabel("Select Instrument:");
		lblSelectInstr.setHorizontalAlignment(SwingConstants.LEFT);
		lblSelectInstr.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblSelectInstr.setBounds(10, 100, 200, 25);
		lblSelectInstr.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[1].add(lblSelectInstr);

		JLabel lblPartName = new JLabel("Part Name:");
		lblPartName.setHorizontalAlignment(SwingConstants.LEFT);
		lblPartName.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblPartName.setBounds(10, 180, 125, 25);
		lblPartName.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[1].add(lblPartName);

		JLabel lblPartLoc = new JLabel("Part Location:");
		lblPartLoc.setHorizontalAlignment(SwingConstants.LEFT);
		lblPartLoc.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblPartLoc.setBounds(10, 260, 125, 25);
		lblPartLoc.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[1].add(lblPartLoc);

		intSelectButtons[0].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = jTables[NUM_OF_DATA_TABS].getSelectedRow();
				modelPieces = (DefaultTableModel) jTables[NUM_OF_DATA_TABS].getModel();
				pieceSelected = modelPieces.getValueAt(row, 0);
				btnPieceNotSelected.setText("Piece: " + pieceSelected);
				setEnabledRecursive(tabbedPane, true);
				internalFrames[0].dispose();
			}
		});
		intSelectButtons[1].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = jTables[1 + NUM_OF_DATA_TABS].getSelectedRow();
				modelInstruments = (DefaultTableModel) jTables[1 + NUM_OF_DATA_TABS].getModel();
				instrumentSelected = modelInstruments.getValueAt(row, 0);
				btnInstrumentNotSelected.setText("Instrument: " + instrumentSelected);
				setEnabledRecursive(tabbedPane, true);
				internalFrames[1].dispose();
			}
		});

		
		txtPartName = new JTextField();
		txtPartName.setColumns(10);
		txtPartName.setBounds(132, 180, 361, 25);
		mainPanelsLeft[1].add(txtPartName);

		txtPartLoc = new JTextField();
		txtPartLoc.setColumns(10);
		txtPartLoc.setBounds(132, 260, 361, 25);
		mainPanelsLeft[1].add(txtPartLoc);


		

		// 			*Licenses
		JLabel lblSelectPart = new JLabel("Select Part:");
		lblSelectPart.setHorizontalAlignment(SwingConstants.LEFT);
		lblSelectPart.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblSelectPart.setBounds(10, 100, 125, 25);
		lblSelectPart.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[2].add(lblSelectPart);
		
		JLabel lblSelectUser = new JLabel("Select User:");
		lblSelectUser.setHorizontalAlignment(SwingConstants.LEFT);
		lblSelectUser.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblSelectUser.setBounds(10, 20, 125, 25);
		lblSelectUser.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[2].add(lblSelectUser);
		
		JLabel lblField = new JLabel("Start Date:");
		lblField.setHorizontalAlignment(SwingConstants.LEFT);
		lblField.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblField.setBounds(10, 180, 125, 25);
		lblField.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[2].add(lblField);

		JLabel lblField_1 = new JLabel("End Date:");
		lblField_1.setHorizontalAlignment(SwingConstants.LEFT);
		lblField_1.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblField_1.setBounds(10, 260, 125, 25);
		lblField_1.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[2].add(lblField_1);
		
		//Generate days months and years arrays as values for comboboxes
		Integer valueToAdd = 0;
		
		Object[] days = new Object[31];
		Object[] months = new Object[12];
		Object[] years = new Object[101];
		
		for (Integer i = 1; i <= 9; i++) {
			valueToAdd = i;
			days[i-1] = "0" + valueToAdd.toString();
			months[i-1] = "0" + valueToAdd.toString();
		}
		
		for (Integer i = 10; i <= days.length; i++) {
			valueToAdd = i;
			days[i-1] = valueToAdd.toString();
		}
		
		for (Integer i = 10; i <= months.length; i++) {
			valueToAdd = i;
			months[i-1] = valueToAdd.toString();
		}
		
		for (Integer i = 0; i <= 100; i++) {
			valueToAdd = i + 2000;
			years[i] = valueToAdd.toString();
		}
		
		cmbStartDateDay = new JComboBox(days);
		cmbStartDateMonth = new JComboBox(months);
		cmbStartDateYear = new JComboBox(years);
		
		cmbStartDateYear.setBounds(120, 180, 100, 25);
		cmbStartDateMonth.setBounds(240, 180, 100, 25);
		cmbStartDateDay.setBounds(360, 180, 100, 25);
		mainPanelsLeft[2].add(cmbStartDateYear);
		mainPanelsLeft[2].add(cmbStartDateMonth);
		mainPanelsLeft[2].add(cmbStartDateDay);
		
		cmbEndDateDay = new JComboBox(days);
		cmbEndDateMonth = new JComboBox(months);
		cmbEndDateYear = new JComboBox(years);
		
		cmbEndDateYear.setBounds(120, 260, 100, 25);
		cmbEndDateMonth.setBounds(240, 260, 100, 25);
		cmbEndDateDay.setBounds(360, 260, 100, 25);
		mainPanelsLeft[2].add(cmbEndDateYear);
		mainPanelsLeft[2].add(cmbEndDateMonth);
		mainPanelsLeft[2].add(cmbEndDateDay);

		btnUserNotSelected = new JButton("User: Not Selected");
		btnUserNotSelected.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				internalFrames[3].setEnabled(true);
				internalFrames[3].setVisible(true);
				bothPanelsBot[3 + NUM_OF_DATA_TABS].setVisible(true);
				scrollPanes[3 + NUM_OF_DATA_TABS].setEnabled(true);
				scrollPanes[3 + NUM_OF_DATA_TABS].setVisible(true);
				jTables[3 + NUM_OF_DATA_TABS].setEnabled(true);
				jTables[3 + NUM_OF_DATA_TABS].setVisible(true);
				setEnabledRecursive(tabbedPane, false);
			}
		});
		btnUserNotSelected.setBounds(120, 20, 200, 25);
		mainPanelsLeft[2].add(btnUserNotSelected);
		
		intSelectButtons[3].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = jTables[3 + NUM_OF_DATA_TABS].getSelectedRow();
				modelUsers = (DefaultTableModel) jTables[3 + NUM_OF_DATA_TABS].getModel();
				userNameSelected = modelUsers.getValueAt(row, 0);
				btnUserNotSelected.setText("User: " + userNameSelected);
				setEnabledRecursive(tabbedPane, true);
				internalFrames[3].dispose();
			}
		});

		btnPartNotSelected = new JButton("Part: Not Selected");
		btnPartNotSelected.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				internalFrames[2].setEnabled(true);
				internalFrames[2].setVisible(true);
				bothPanelsBot[2 + NUM_OF_DATA_TABS].setVisible(true);
				scrollPanes[2 + NUM_OF_DATA_TABS].setEnabled(true);
				scrollPanes[2 + NUM_OF_DATA_TABS].setVisible(true);
				jTables[2 + NUM_OF_DATA_TABS].setEnabled(true);
				jTables[2 + NUM_OF_DATA_TABS].setVisible(true);
				setEnabledRecursive(tabbedPane, false);
			}
		});
		btnPartNotSelected.setBounds(120, 100, 200, 25);
		mainPanelsLeft[2].add(btnPartNotSelected);
		
		intSelectButtons[2].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = jTables[2 + NUM_OF_DATA_TABS].getSelectedRow();
				
				modelParts = (DefaultTableModel) jTables[2 + NUM_OF_DATA_TABS].getModel();
				partIDSelected = modelParts.getValueAt(row, 0);
				btnPartNotSelected.setText("Part: " + partIDSelected);
				setEnabledRecursive(tabbedPane, true);
				internalFrames[2].dispose();
			}
		});

		

		
		// 			*Users

		JLabel lblNewLabel = new JLabel("Username:");
		lblNewLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblNewLabel.setBounds(10, 20, 125, 25);
		lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[3].add(lblNewLabel);

		JLabel label = new JLabel("Password:");
		label.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		label.setHorizontalAlignment(SwingConstants.LEFT);
		label.setBounds(10, 100, 125, 25);
		label.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[3].add(label);
		
		JLabel labelPassCheck = new JLabel("Check Pword:");
		labelPassCheck.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		labelPassCheck.setHorizontalAlignment(SwingConstants.LEFT);
		labelPassCheck.setBounds(10, 180, 125, 25);
		labelPassCheck.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[3].add(labelPassCheck);

		JLabel label_1 = new JLabel("First Name:");
		label_1.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		label_1.setHorizontalAlignment(SwingConstants.LEFT);
		label_1.setBounds(10, 260, 125, 25);
		label_1.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[3].add(label_1);

		JLabel label_2 = new JLabel("Last Name:");
		label_2.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		label_2.setHorizontalAlignment(SwingConstants.LEFT);
		label_2.setBounds(10, 340, 125, 25);
		label_2.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[3].add(label_2);

		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setHorizontalAlignment(SwingConstants.LEFT);
		lblEmail.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblEmail.setBounds(10, 420, 125, 25);
		lblEmail.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[3].add(lblEmail);
		
		JLabel lblUserType = new JLabel("User Type:");
		lblUserType.setHorizontalAlignment(SwingConstants.LEFT);
		lblUserType.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblUserType.setBounds(10, 500, 361, 25);
		lblUserType.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[3].add(lblUserType);
		
		txtUsername = new JTextField();
		txtUsername.setBounds(130, 20, 361, 25);
		mainPanelsLeft[3].add(txtUsername);
		txtUsername.setColumns(10);

		pswPassword = new JPasswordField();
		pswPassword.setBounds(130, 100, 361, 25);
		mainPanelsLeft[3].add(pswPassword);
		
		pswPassCheck = new JPasswordField();
		pswPassCheck.setBounds(130, 180, 361, 25);
		mainPanelsLeft[3].add(pswPassCheck);

		txtFirstname = new JTextField();
		txtFirstname.setColumns(10);
		txtFirstname.setBounds(130, 260, 361, 25);
		mainPanelsLeft[3].add(txtFirstname);

		txtLastname = new JTextField();
		txtLastname.setColumns(10);
		txtLastname.setBounds(130, 340, 361, 25);
		mainPanelsLeft[3].add(txtLastname);

		txtEmail = new JTextField();
		txtEmail.setColumns(10);
		txtEmail.setBounds(130, 420, 361, 25);
		mainPanelsLeft[3].add(txtEmail);
		
		final Object[] userTypes = new Object[] {"User", "Admin"}; 
		cmbUserType = new JComboBox(userTypes);
		cmbUserType.setBounds(130, 500, 361, 25);
		mainPanelsLeft[3].add(cmbUserType);
		// 			*Instruments

		JLabel lblInstrName = new JLabel("Instrument Name:");
		lblInstrName.setHorizontalAlignment(SwingConstants.LEFT);
		lblInstrName.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblInstrName.setBounds(10, 20, 151, 25);
		lblInstrName.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[4].add(lblInstrName);

		JLabel lblInstrBand = new JLabel("Instrument Band:");
		lblInstrBand.setHorizontalAlignment(SwingConstants.LEFT);
		lblInstrBand.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
		lblInstrBand.setBounds(10, 100, 151, 25);
		lblInstrBand.setForeground(new Color(255, 255, 255));
		mainPanelsLeft[4].add(lblInstrBand);

		txtInstrName = new JTextField();
		txtInstrName.setColumns(10);
		txtInstrName.setBounds(190, 20, 295, 25);
		mainPanelsLeft[4].add(txtInstrName);

		cmbInstrBand = new JComboBox();
		cmbInstrBand.addItem("BSB");
		cmbInstrBand.addItem("Swing");
		cmbInstrBand.setBounds(190, 100, 295, 25);
		mainPanelsLeft[4].add(cmbInstrBand);
		
		txtPieces = new JTextField[] {txtExists, txtTitle, txtArranger, txtComposer, txtPublisher, txtXmas,
				txtSx, txtMin, txtSec, txtComment, txtCw, txtMusicType, txtEthnic, txtRhythm };
		txtUsers = new JTextField[] {txtUsername, txtEmail, txtLastname, txtFirstname };
		txtLicenses = new JTextField[] {};
		txtParts = new JTextField[] {txtPartName, txtPartLoc};
		txtInstruments = new JTextField[] {txtInstrName};
		txtSets = new JTextField[][] {txtPieces, txtParts, txtLicenses, txtUsers, txtInstruments};
		
		//Show dialogue box to ensure user wants to exit
		frame.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		        if (JOptionPane.showConfirmDialog(frame, 
		            "Are you sure you want to close this window?", "Close Window?", 
		            JOptionPane.YES_NO_OPTION,
		            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
		        	connection.closeConnection();
		            System.exit(0);
		        }
		    }
		});
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		update();
		frame.setVisible(true);

	}

	//*End of initialisation
	
	/**
	 * Displays a table of data from a database connection to a JTable with
	 * un-editable cells.
	 */
	private void update() {
		for (int i = 0; i < jTables.length; i++) {
			jTables[i].setModel(new DefaultTableModel(connection.returnDBData(trueTableNames[i]), columnNames[i]) {
				public boolean isCellEditable(int row, int column) {
					return false;
				}
			});
		}
	}

	/**
	 * Fills the input text boxes with information from the table
	 * 
	 * @param ID_Num - The identifier for the 'table'
	 */
	private void fillTextBoxes(int ID_Num) {
		JTable table = jTables[ID_Num];
		JTextField[] txtArray = txtSets[ID_Num];
		if (table.getRowCount() > 0 && table.getSelectedRow() != -1) {
			switch (ID_Num) {
				case 0:
					for (int i = 0; i < txtArray.length; i++) {
						txtArray[i].setText((String) table.getValueAt(table.getSelectedRow(), i + 1));
					}
					break;
					
				case 1:
					pieceSelected = table.getValueAt(table.getSelectedRow(), 2);
					instrumentSelected = table.getValueAt(table.getSelectedRow(), 4);
					btnPieceNotSelected.setText("Piece: " + pieceSelected);
					btnInstrumentNotSelected.setText("Instrument: " + instrumentSelected);
					txtArray[0].setText((String) table.getValueAt(table.getSelectedRow(), 1));
					txtArray[1].setText((String) table.getValueAt(table.getSelectedRow(), 3));
					break;
					
				case 2:
					userNameSelected = table.getValueAt(table.getSelectedRow(), 1);
					partIDSelected = table.getValueAt(table.getSelectedRow(), 2);
					btnUserNotSelected.setText("User: " + userNameSelected);
					btnPartNotSelected.setText("Part: " + partIDSelected);
					
					String[] startDate = ((String) table.getValueAt(table.getSelectedRow(), 3)).split("-");
					cmbStartDateYear.setSelectedItem(startDate[0]);
					cmbStartDateMonth.setSelectedItem(startDate[1]);
					cmbStartDateDay.setSelectedItem(startDate[2]);
					String[] endDate = ((String) table.getValueAt(table.getSelectedRow(), 4)).split("-");
					cmbEndDateYear.setSelectedItem(endDate[0]);
					cmbEndDateMonth.setSelectedItem(endDate[1]);
					cmbEndDateDay.setSelectedItem(endDate[2]);
					break;
					
				case 3:
					txtArray[0].setText((String) table.getValueAt(table.getSelectedRow(), 0));
					
					for (int i = 3; i > 0; i--) {
						txtArray[i].setText((String) table.getValueAt(table.getSelectedRow(), i));
					}
					cmbUserType.setSelectedItem(table.getValueAt(table.getSelectedRow(), 4));
					break;
				
				case 4:
					txtArray[0].setText((String) table.getValueAt(table.getSelectedRow(), 1));
					cmbInstrBand.setSelectedItem((String) table.getValueAt(table.getSelectedRow(), 2));
			}
			
		}
	}

	/**
	 * Deletes one or multiple rows from the table
	 * 
	 * @param table
	 *            - JTable to hold the information in the GUI
	 * @param tableName
	 *            - table in the database to retrieve information from
	 */
	private void deleteRow(JTable table, String tableName) {
		int[] rowsToDelete = table.getSelectedRows();
		DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
		// If a row is selected
		if (rowsToDelete.length != 0) {	
			if(ErrorBox.confirmBox(frame, "Are you sure you wish to delete?", "Confirm") == 0) {
				String[] IDsToDelete = new String[rowsToDelete.length];
				//get the IDs, rather than the row number
				for (int i = 0; i < rowsToDelete.length; i++) {
					IDsToDelete[i] = tableModel.getValueAt(rowsToDelete[i], 0).toString();
				}
				//clear the selection, remove the item and update the table
				table.clearSelection();
				connection.removeItem(tableName, IDsToDelete);
				update();
			}
		} else {
			ErrorBox.createErrorBox(frame, "No row selected!", "Error", 0);
		}
	}
	
	
	/**
	 * Retrieves the data from the input fields and selected IDs, in the case of edit
	 * @param table - The table to retrieve selected info from
	 * @param IDnew - If false, get the ID from table for edit, if true, make a new int for add
	 * @param table - The name of the table in question
	 * @return a list of all the fields needed including ID
	 */
	private List<Object> getEditEntries(JTable table, Boolean IDnew, String tableName) {
		List<Object> dataToInsert = new ArrayList<Object>();
		DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
		//If the ID is a new one (adding)
		if (IDnew) {
			if (tableName != "bandusers") {
				dataToInsert.add(Connect.getLastID(tableName) + 1);
			}
		} 
		//If the ID is the selected one (editing)
		else {
			int[] rowsToEdit = table.getSelectedRows();
			String[] IDsToEdit = new String[rowsToEdit.length];
			//Get the IDs from the row numbers selected
			for (int i = 0; i < rowsToEdit.length; i++) {
				IDsToEdit[i] = tableModel.getValueAt(rowsToEdit[i], 0).toString();
			}
			table.clearSelection(); 
			dataToInsert.add(IDsToEdit);
			//dataToInsert.add(tableModel.getValueAt(table.getSelectedRow(),0));
		}
		
		//add the other values based on the table
		switch (tableName) {
			case "bandusers":
				byte[] newSalt = Hash.getSalt();
				
				if (IDnew) {	
					dataToInsert.add(txtUsers[0].getText());
					dataToInsert.add(Hash.hashWithSalt(Hash.convertPassword(pswPassword.getPassword()), newSalt));
					dataToInsert.add(Hex.encodeHexString(newSalt));
				} else {
					dataToInsert.add("");
					dataToInsert.add("");
				}
				for (int i = 1; i < txtUsers.length; i++) {
					dataToInsert.add(txtUsers[i].getText());
				}
				dataToInsert.add(cmbUserType.getSelectedItem().toString());
				break; 
			case "pieces":
				for (int i = 0; i < txtPieces.length; i++) {
					try {
						dataToInsert.add(Integer.parseInt(txtPieces[i].getText()));
					} catch (Exception NumberFormatException) {
						dataToInsert.add(txtPieces[i].getText());
					}
				}
				break;
			case "licenses":
				dataToInsert.add(userNameSelected);
				dataToInsert.add(partIDSelected);
				dataToInsert.add(cmbStartDateYear.getSelectedItem().toString() + "-" + cmbStartDateMonth.getSelectedItem().toString()
						+ "-" + cmbStartDateDay.getSelectedItem().toString());
				dataToInsert.add(cmbEndDateYear.getSelectedItem().toString() + "-" + cmbEndDateMonth.getSelectedItem().toString()
						+ "-" + cmbEndDateDay.getSelectedItem().toString());
				dataToInsert.add(0);
				break;
			case "instruments":
				dataToInsert.add(txtInstruments[0].getText());
				dataToInsert.add(cmbInstrBand.getSelectedItem());
				break;
			case "pieceparts":
				dataToInsert.add(txtPartName.getText());
				dataToInsert.add(pieceSelected);
				dataToInsert.add(txtPartLoc.getText());
				dataToInsert.add(instrumentSelected);
				break;
		}
		return dataToInsert;
	}
	
	/**
	 * Recursively disables/enables all JTextFields, JButtons, JTables and JComboBoxes in a container component
	 * @param container - the container whose contents are to be disabled
	 * @param enableStatus - Set to true to enable the components, false to disable
	 */
	void setEnabledRecursive(Container container, Boolean enableStatus) {
	    for (Component c : container.getComponents()) {
	        if (c instanceof JTextField || c instanceof JButton || c instanceof JTable || c instanceof JComboBox) {
	           c.setEnabled(enableStatus);
	        } else
	        if (c instanceof Container) {
	        	setEnabledRecursive((Container)c, enableStatus);
	        }
	    }
	}
	
	/**
	 * Sets the text of all textfields given to empty
	 * @param fields - An array of the textfields to clear 
	 */
	private void clearTextBoxes(JTextField[] fields) {
		for(int i = 0; i < fields.length; i++) {
			fields[i].setText("");
		}
	}
	
	/**
	 * Verifies the input data is valid before running add or edit
	 * @param i - the identifier for the 'table'
	 * @param addOrEdit - The action being performed with the data
	 */
	private void Checks(int i, String addOrEdit) {
		boolean allChecksGood = true;
		boolean passGood = false;
		if(addOrEdit.equals("add")) {
			if (pswPassword.getPassword().length == pswPassCheck.getPassword().length) {
				passGood = true;
				for (int j = 0; j < pswPassword.getPassword().length; j++) {
					if (pswPassword.getPassword()[j] == pswPassCheck.getPassword()[j]) {
						passGood = true;
					}
				}
			} 
		} else if(addOrEdit.equals("edit")) {
			passGood = true;
			if(pswPassword.getPassword().length != 0) {
				if (pswPassword.getPassword().length == pswPassCheck.getPassword().length) {
					passGood = true;
					for (int j = 0; j < pswPassword.getPassword().length; j++) {
						if (pswPassword.getPassword()[j] == pswPassCheck.getPassword()[i]) {
							passGood = true;
						} else {
							passGood = false;
						}
					}
				} 
			}
		}
		
		if(trueTableNames[i] == "licenses") {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String startDate = (String) cmbStartDateYear.getSelectedItem() + "-" + cmbStartDateMonth.getSelectedItem() + "-" + cmbStartDateDay.getSelectedItem();
			String endDate = (String) cmbEndDateYear.getSelectedItem() + "-" + cmbEndDateMonth.getSelectedItem() + "-" + cmbEndDateDay.getSelectedItem();
			Date date1 = null;
			Date date2 = null;
			try {
				date1 = sdf.parse(startDate);
				date2 = sdf.parse(endDate);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if(date1.after(date2)) {
				allChecksGood = false;
				ErrorBox.createErrorBox(frame, "Please enter a valid start date", "Invalid Data", 0);
			}
			LocalDate dateLocal = LocalDate.now();
			Date date3 = Date.from(dateLocal.atStartOfDay(ZoneId.systemDefault()).toInstant());
			if(date3.after(date2) && allChecksGood) {
				allChecksGood = false;
				ErrorBox.createErrorBox(frame, "Please enter a date after the current date", "Invalid Data", 0);
			}
			if(userNameSelected.equals("") || partIDSelected.equals("")) {
				allChecksGood = false;
				ErrorBox.createErrorBox(frame, "Please select a user and a part", "Invalid Data", 0);
			}
		} else if (trueTableNames[i].equals("bandusers")) {
			if(txtUsername.getText().equals("")) {
				allChecksGood = false;
				ErrorBox.createErrorBox(frame, "Please enter a username", "Invalid Data", 0);
			}
			if(!passGood) {
				ErrorBox.createErrorBox(frame, "Password Mismatch", "Error", 0);
				allChecksGood = false;
			}
			
			if(passGood) {
				boolean emptyField = false;
				for(int j = 1; j < txtUsers.length; j++) {
					if(txtUsers[j].getText().equals("")) {
						emptyField = true;
					}
				}
				if(emptyField) {
					if(ErrorBox.confirmBox(frame, "Are you sure you want to enter empty data?", "Empty data") == 1) {
						allChecksGood = false;
					}	
				}
			}
		} else if(trueTableNames[i].equals("instruments")) {
			if(txtInstrName.getText().equals("")) {
				allChecksGood = false;
				ErrorBox.createErrorBox(frame, "Please enter an instrument name", "Invalid Data", 0);
			}
		} else if(trueTableNames[i].equals("pieces")) {
			try {
				if(Integer.parseInt(txtSec.getText()) > 60) {
					allChecksGood = false;
					ErrorBox.createErrorBox(frame, "Please enter a valid time", "Invalid Data", 0);
				}
			} catch (NumberFormatException e) {
				allChecksGood = false;
				ErrorBox.createErrorBox(frame, "Please enter a valid time", "Invalid Data", 0);
			}
				
			String year = txtCw.getText();
			try {
				Integer yearInt = Integer.parseInt(year);
				if(year.length() != 4 || yearInt > 9999 || yearInt < 0) {
					allChecksGood = false;
					ErrorBox.createErrorBox(frame, "Please enter a valid year", "Invalid Data", 0);
				}
			} catch(NumberFormatException e ) {
				allChecksGood = false;
				ErrorBox.createErrorBox(frame, "Please enter a valid year", "Invalid Data", 0);
			}
			
			
			if(txtExists.getText().length() >= 1) {
				String exists = txtExists.getText();
				if(!exists.equals("y")  && !exists.equals("Y")) {
					allChecksGood = false;
					ErrorBox.createErrorBox(frame, "Please enter only (\"y\" or \"Y\") for exists", "Invalid Data", 0);
				}
			}

			if(allChecksGood) {
				boolean emptyFields = false;
				for(int j = 0; j < txtPieces.length; j++) {
					if(txtPieces[i].getText().equals("")) {
						emptyFields = true;
					}
				}
				if(emptyFields) {
					if(ErrorBox.confirmBox(frame, "Are you sure you want to enter empty data", "Empty data") == 1) {
						allChecksGood = false;
					}
				}
			}
		} else if(trueTableNames[i].equals("pieceparts")) {
			if(instrumentSelected.equals("") || pieceSelected.equals("")) {
				allChecksGood = false;
				ErrorBox.createErrorBox(frame, "Please select and instrument and piece", "Invalid Data", 0);
			}
			
			if(txtPartName.getText().equals("")) {
				allChecksGood = false;
				ErrorBox.createErrorBox(frame, "Please enter a part name", "Invalid Data", 0);
			}
			boolean locNotEmpty = false;
			if(txtPartLoc.getText().equals("")) {
				allChecksGood = false;
				ErrorBox.createErrorBox(frame, "Please enter a part location", "Invalid Data", 0);
				locNotEmpty = true;
			}
			if (locNotEmpty){
				String[] pdf = txtPartLoc.getText().split("\\.");
				if(pdf.length < 1) {
					allChecksGood = false; 
					ErrorBox.createErrorBox(frame, "Please enter a .pdf title", "Invalid Data", 0);
				}
			}
			
			
		}
		
		
		//If at any one of the checks found the data to be invalid, the action will not be performed
		if (allChecksGood && addOrEdit.equals("add")) {
			// Retrieves and formats the data from the text boxes
			List<Object> dataToInsert = getEditEntries(jTables[i], true, trueTableNames[i]);
			//Adds the data to the table via SQL
			connection.addNewEntry(dataToInsert, trueTableNames[i], frame);
			
			update();
		} else if(allChecksGood && addOrEdit.equals("edit")) {
			int row = jTables[i].getSelectedRow();
			// Retrieves and formats the data from the text boxes
			List<Object> dataToInsert = getEditEntries(jTables[i], false, trueTableNames[i]);
			//Adds the data to the table via SQL
			
			connection.updateEntry(dataToInsert, trueTableNames[i], frame);
			update();
			jTables[i].setRowSelectionInterval(row, row);
		}
		
	}
}
