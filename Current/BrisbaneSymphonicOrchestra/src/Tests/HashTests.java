package Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import Security.Hash;

public class HashTests {

	@Test
	public void testSaltCreation() {
		byte[] salt = Hash.getSalt();
		assertEquals(16, salt.length);
	}
	
	@Test
	public void testPasswordHash() {
		byte[] salt = Hash.getSalt();
		String password = Hash.hashWithSalt("secret", salt);
		System.out.println();
		assertEquals(64, password.length());
	}
	
	@Test
	public void testConvertPass() {
		char[] pass = {'p', 'a', 's', 's', 'w', 'o', 'r', 'd'};
		String password = Hash.convertPassword(pass);
		assertTrue(password.equals("password"));
	}
}
