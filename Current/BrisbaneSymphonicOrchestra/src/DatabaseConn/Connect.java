package DatabaseConn;
import java.awt.EventQueue;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;

import org.apache.commons.codec.binary.Hex;

import com.mysql.jdbc.Security;
import com.mysql.jdbc.Statement;

import GUI.LoginPage;
import Security.ErrorBox;
import Security.Hash;

public class Connect {
	private static final int SALT_FIELD = 2;
	private static final int PASSWORD_FIELD = 1;
	private static Connection connection = null;
	private static String user = null;
	private static String[] tableNames = new String[] {"pieces", "bandusers", "licenses", "instruments", "pieceparts"};
	private static Hashtable<String, String[]> columnNames = new Hashtable<String, String[]>();
	
	/**
	 * Creates an instance of a connection to a database.
	 */
	public Connect() throws SQLException {
			String url = "jdbc:mysql://localhost:3306/brisbane_symphonic_orchestra";
			String userName = "root";
			String password = "";
			connection = DriverManager.getConnection(url, userName, password);
			Statement stmt = (Statement)connection.createStatement();
			//Retrieve the names of the column names from the database
			for (String tableName:tableNames) {
				String query = "SELECT COUNT(COLUMN_NAME) FROM information_schema.COLUMNS WHERE TABLE_NAME = '" + tableName + "'";
				ResultSet rs = stmt.executeQuery(query);
				String[] tableColumnNames = null;
				while (rs.next()) {
					tableColumnNames = new String[rs.getInt("COUNT(COLUMN_NAME)")];
				}
				query = "SELECT COLUMN_NAME FROM information_schema.COLUMNS WHERE TABLE_NAME = '" + tableName + "'";
				rs = stmt.executeQuery(query);
				int i = 0;
				while (rs.next()) {
					tableColumnNames[i] = rs.getString("COLUMN_NAME");
					i++;
				}
				columnNames.put(tableName, tableColumnNames);
			}
		
	}
	
	/**
	 * Returns the information only important to the user
	 * @return - A 2D array of objects containing the information from the database
	 */
	public static Object[][] getUserViewData() {
		Object[][] userViewData = null;
		try {
			Statement stmt = (Statement)connection.createStatement();
			int rows = 0;
			String query = "SELECT COUNT(licenses.licenseID) AS count FROM licenses WHERE username = '" + user + "'";
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				rows = rs.getInt("count");
			}
			query = "SELECT pieces.Title, instruments.instrumentname, licenses.startLicenseDate, licenses.endLicenseDate, pieceparts.part_loc, instruments.instrumentband , licenses.licenseID "
					+ "FROM pieces, instruments, licenses, pieceparts WHERE licenses.username = '" + user + "' AND licenses.part_id = "
							+ "pieceparts.pieceparts_id AND pieceparts.piece_id = pieces.pieces_id AND pieceparts.instrument_id = "
							+ "instruments.instrument_id";
			
			userViewData = new Object[rows][7];
			rs = stmt.executeQuery(query);
			int i = 0;
			while(rs.next()) {
				
				userViewData[i][0] = rs.getString("Title");
				userViewData[i][1] = rs.getString("instrumentname");
				userViewData[i][2] = rs.getString("startLicenseDate");
				userViewData[i][3] = rs.getString("endLicenseDate");
				userViewData[i][4] = rs.getString("instrumentband");
				userViewData[i][5] = rs.getString("part_loc");
				userViewData[i][6] = rs.getInt("licenseID");
				i++;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userViewData;
	}
	
	/**
	 * Find a user's salt using their username in the DB.
	 * @param username: The specified user's username to search for in the DB.
	 * @return a byte array containing the user's salt from the DB.
	 */
	public static byte[] findUserSalt(String username) {
		String salt = null;
		Statement stmt;
		String query = "SELECT salt FROM bandusers WHERE userName = '" + username + "'";
		byte[] byteSalt = null;
		try {
			stmt = (Statement)connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while  (rs.next()) {
				salt = rs.getString("salt");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		user = username;
		if (salt != null) {
			byteSalt = Hash.hexStringToByteArray(salt);
		}
		return byteSalt;
	}
	
	/**
	 * Compares a user's password entered at runtime to the password stored in the DB.
	 * @param username: The user's username to compare password for.
	 * @param password: The plaintext password entered at runtime.
	 * @param salt: The salt for the specified user to hash the plaintext password and compare.
	 * @return true if the password is correct, otherwise returns false
	 */
	public static boolean comparePassword(String username, String password, byte[] salt) {
		boolean matches = false;
		Statement stmt;
		String storedPassword = null;
		String hashedPassword = Hash.hashWithSalt(password, salt);
		String query = "SELECT password FROM bandusers WHERE userName = '" + username + "'";
		try {
			stmt = (Statement)connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while  (rs.next()) {
				storedPassword = rs.getString("password");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		matches = storedPassword.equals(hashedPassword);
		return matches;
	}
	
	/**
	 * Determine the type of user, i.e. Admin or User.
	 * @param username: The username of the user. 
	 * @return: The type of user as String
	 */
	public static String determineUserType(String username) {
		String userType = null;
		Statement stmt;
		String query = "SELECT userType FROM bandusers WHERE userName = '" + username + "'";
		try {
			stmt = (Statement)connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while  (rs.next()) {
				userType = rs.getString("userType");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userType;
	}
	
	/**
	 * Get the current user logged in.
	 * @return: The username of the currently logged in user as String.
	 */
	public static String returnUser() {
		return user;
	}
	
	/**
	 * Get the column names for a specified table in the DB.
	 * @param table: The table to be searched for in the DB.
	 * @return: An array of String's of the column names.
	 */
	public static String[] returnColumnNames(String table) {
		if (table == "bandusers") {
			return new String[] {"username", "email", "lastName", "firstName", "userType"};
		} else {
			return columnNames.get(table);
		}
		
	}
	
	/**
	 * Get the data contained in a table in the DB to populate a JTable.
	 * @param tableName: The table to be searched for in the DB.
	 * @return: A 2D array of Strings of the data in the DB.
	 */
	public static Object[][] returnDBData(String tableName) {
		Object[][] data = new Object[returnRowCount(tableName)][columnNames.get(tableName).length];
		Statement stmt;
		String query = "SELECT * FROM " + tableName;
		try {
			stmt = (Statement)connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			int i = 0;
			if (tableName == "bandusers") {
				while  (rs.next()) {
					data[i][0] = rs.getString(columnNames.get(tableName)[0]);
					//Skip password and Salt, admin doesn't need to see these as they are incomprehensible
					for (int j = 3;j < columnNames.get(tableName).length; j++) {
						data[i][j-2] = rs.getString(columnNames.get(tableName)[j]);
					}
				i++;
				}
			} else {
				while  (rs.next()) {
					for (int j = 0;j < columnNames.get(tableName).length; j++) {
						data[i][j] = rs.getString(columnNames.get(tableName)[j]);
					}
				i++;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return data;
	}
	
	/**
	 * Get the data filtered to contain only rows with the search term in a field
	 * @param tableName: The table to be searched in the DB.
	 * @param searchTerm: The term to be searched for.
	 * @param columnToSearch: the name of the column to search for the term in
	 * @return: A 2D array of Strings of the data in the DB.
	 */
	public static Object[][] returnFilteredData(String tableName, String searchTerm, String columnToSearch) {
		Object[][] data = new Object[returnRowCount(tableName)][columnNames.get(tableName).length];
		Statement stmt;
		String query = "SELECT * FROM " + tableName + " WHERE " + columnToSearch + " LIKE '%" + searchTerm + "%'";
		
		try {
			stmt = (Statement)connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			int i = 0;
			if (tableName == "bandusers") {
				while  (rs.next()) {
					data[i][0] = rs.getString(columnNames.get(tableName)[0]);
					//Skip password and Salt, admin doesn't need to see these as they are incomprehensible
					for (int j = 3;j < columnNames.get(tableName).length; j++) {
						data[i][j] = rs.getString(columnNames.get(tableName)[j]);
					}
				i++;
				}
			} else {
				while  (rs.next()) {
					for (int j = 0;j < columnNames.get(tableName).length; j++) {
						data[i][j] = rs.getString(columnNames.get(tableName)[j]);
					}
				i++;
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return data;
	}
	
	/**
	 * Adds a new entry to the DB.
	 * @param entries: An array of objects to insert into the DB.
	 * @param tableName: The table name for the table in the DB to have the data inserted into.
	 * @param givenFrame: The frame corresponding to the table
	 */
	public static void addNewEntry(List<Object> entries, String tableName, JFrame givenFrame) {
		Statement stmt;
		String query = "INSERT INTO " + tableName +  " (";
		query = query.concat(columnNames.get(tableName)[0]);
		//Add the others
		for (int i = 1; i < columnNames.get(tableName).length; i++) {
			query = query.concat(", " + columnNames.get(tableName)[i]);
		}
		//The syntax that precedes the values
		query = query.concat(") VALUES (");
		
		//Add the values (except the last) to the SQL query
		for(int i = 0; i < entries.size() - 1; i++) {
			if(entries.get(i).getClass().equals(String.class)) {
				query = query.concat("\"" + entries.get(i).toString() + "\", ");
			} else {
				query = query.concat(entries.get(i).toString() + ", ");
			}
		}
		//Add the last value with different syntax
		if(entries.get(entries.size() - 1).getClass().equals(String.class)) {
			query = query.concat("\"" + entries.get(entries.size() - 1).toString() + "\")");
		} else {
			query = query.concat(entries.get(entries.size() - 1).toString() + ")");
		}
		
		try {
			stmt = (Statement)connection.createStatement();
			
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			ErrorBox.createErrorBox(givenFrame, "Incorrect data entered", "Error", 0);
			e.printStackTrace();
		}
	}
	
	/**
	 * Get the number of rows for a table in the DB.
	 * @param table: The table to be searched for in the DB.
	 * @return: An int of the number of rows.
	 */
	public static int returnRowCount(String table) {
		int count = 0;
		String query = "SELECT COUNT(*) AS count FROM " + table;
		Statement stmt3;
		try {
			stmt3 = (Statement)connection.createStatement();
			ResultSet rs3 = stmt3.executeQuery(query);
			while(rs3.next()){
			    count = rs3.getInt("count");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}
	
	/**
	 * Removes a specified item and its associated row in the DB.
	 * @param iDs: An array of IDs for rows to be removed from the DB.
	 * @param table: The table to have the data removed from.
	 * @param iD: The ID/Primary Key of the table to have the data removed.
	 * @return: true if removeItem was successful
	 */
	public static boolean removeItem(String tableName, String[] IDsToDelete) {
		boolean successful = false;
		String ID_name = columnNames.get(tableName)[0];
		//Create query with starting syntax and first ID
		String query = "DELETE FROM " + tableName + " WHERE " + ID_name + " IN (\"" + IDsToDelete[0];
		//Add all other IDs (if any)
		for(int i = 1; i < IDsToDelete.length; i++) {
			query = query + "\", \"" + IDsToDelete[i];
		}
		//Add ending syntax
		query = query + "\")";
		Statement stmt3;
		try {
			stmt3 = (Statement)connection.createStatement();
			stmt3.executeUpdate(query);
			successful = true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return successful;
	}
	
	/**
	 * Updates information stored in the DB.
	 * @param entries: An array of objects to be inserted into the DB.
	 * @param table: The table in the DB to change the data.
	 * @param iD: The ID/Primary Key of the table to have data changed.
	 * @param givenFrame: The frame corresponding to the table
	 */
	public static boolean updateEntry(List<Object> entries, String tableName, JFrame givenFrame) {
		Statement stmt;
		boolean successful = false;
		//Add starting syntax of update statement
		String query = "UPDATE " + tableName + " SET ";
		
		//work out which entries are not empty, the first is the ID, which isn't used until the Where clause and cannot be null
		List<Integer> notNullEntries = new LinkedList<Integer>();
		for (int i = 1; i < entries.size(); i++) {
			if (!entries.get(i).equals("")) {
				notNullEntries.add(i);
			}
		}
		//System.out.println(query);
		
		//Do all non-null entries but the last (and the ID, as mentioned before)
		for (int i = 0; i < notNullEntries.size() - 1; i++) {
			int column = notNullEntries.get(i);
			query = query + columnNames.get(tableName)[column] + " = \"" + entries.get(column) + "\", ";
		}
		//System.out.println(query);
		//Add the last entry
		int lastColumn = notNullEntries.get(notNullEntries.size() - 1);
		query = query + columnNames.get(tableName)[lastColumn] + " = \"" + entries.get(lastColumn);
		//Add syntax for IDs
		query = query + "\" WHERE " + columnNames.get(tableName)[0] + " IN (";
		
		//Add all IDs except last
		String[] IDs = (String[]) entries.get(0);
		//System.out.println(IDs[0]);
		for (int i = 0; i < IDs.length - 1; i++) {
			query = query + "\"" + IDs[i] + "\",";
		}
		//Add last ID and closing syntax
		query = query + "\"" + IDs[IDs.length - 1] + "\"" + ")";
		
		
		try {
			stmt = (Statement)connection.createStatement();
			stmt.executeUpdate(query);
			successful = true;
		} catch (SQLException e) {
			ErrorBox.createErrorBox(givenFrame, "Incorrect data entered", "Error", 0);
			e.printStackTrace();
		}
		return successful;
	}
	
	
	
	/**
	 * Gets the last Primary Key for a table in the DB. Mainly used for tables with 
	 * integer PK's.
	 * @param tableName: The name of the table to get the data for.
	 * @return: An int of the last PK in the DB.
	 */
	public static int getLastID(String tableName) {
		int highestID = 0;
		Statement stmt;
		String query = "SELECT * FROM " + tableName + " ORDER BY " + columnNames.get(tableName)[0] + " DESC LIMIT 1";
		try {
			stmt = (Statement)connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while  (rs.next()) {
				highestID = rs.getInt(columnNames.get(tableName)[0]);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		return highestID;
	}
	
	/**
	 * Gets the total of all of the specified user's printcounts 
	 * @param username: The username of the user who's printcount sum will be returned
	 * @return: the total printcount of the user
	 */
	public static Object[][] returnPrintCount(String username) {
		Object count[][] = null;
		Statement stmt = null;
		int rows = 0;
		String query = "SELECT COUNT(licenseID) AS count FROM licenses WHERE username = '" + username + "'";
		String query2 = "SELECT licenseID, printCounter FROM licenses WHERE username = '" + username + "'";
		try {
			stmt = (Statement)connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				rows = rs.getInt("count");
			}
			rs = stmt.executeQuery(query2);
			int i = 0;
			count = new Object[rows][2];
			while  (rs.next()) {
				count[i][0] = rs.getString("licenseID");
				count[i][1] = rs.getInt("printCounter");
				i++;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return count;
	}
	
	
	/**
	 * Updates the printcount in the database 
	 * @param license: The ID of the license 
	 * @param license: The corresponding printcount
	 * @return: the total printcount of the user
	 */
	public static void updatePrintCount(Object license, Object printCount) {
		String query = "UPDATE licenses SET printCounter = " + printCount + " WHERE licenseID = " + license; 
		Statement stmt;
		try {
			stmt = (Statement)connection.createStatement();
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Returns the Titles of the PDFs that the specified user has access to 
	 * @param username: The username of the user 
	 * @param license: The corresponding printcount
	 * @return: ArrayList containing the PDF titles
	 */
	public static ArrayList<String> getPDFTitles(String username) {
		ArrayList<String> list = new ArrayList<String>();
		String query = "SELECT pieceparts.part_loc FROM pieceparts INNER JOIN licenses ON "
				+ "pieceparts.pieceparts_id=licenses.part_id WHERE licenses.username = '" + username + "'";
		Statement stmt;
		try {
			stmt = (Statement)connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next()) {
				list.add(rs.getString("part_loc"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	
	/**
	 * Returns the number of all the licenses in the database
	 * @return: total number of licenses
	 */
	public int returnLicenseCount() {
		Statement stmt;
		int rows = 0;
		String query = "SELECT COUNT(licenses.licenseID) AS count FROM licenses";
		ResultSet rs;
		try {
			stmt = (Statement)connection.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				rows = rs.getInt("count");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rows;
	}
	
	/**
	 * Closes a connection to the DB. 
	 */
	public static void closeConnection() {
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
