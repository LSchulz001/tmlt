import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;

public class Gui {

	protected Shell shlBrisbaneSymphonicBand;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Gui window = new Gui();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shlBrisbaneSymphonicBand.open();
		shlBrisbaneSymphonicBand.layout();
		while (!shlBrisbaneSymphonicBand.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlBrisbaneSymphonicBand = new Shell();
		shlBrisbaneSymphonicBand.setSize(450, 300);
		shlBrisbaneSymphonicBand.setText("Brisbane Symphonic Band");
		
		Button btnDownloadPdf = new Button(shlBrisbaneSymphonicBand, SWT.NONE);
		btnDownloadPdf.addMouseListener(new MouseAdapter() {
			public void mouseDown(MouseEvent arg0) {
				Download pdf = new Download();
			}
		});
		btnDownloadPdf.setBounds(106, 10, 90, 25);
		btnDownloadPdf.setText("Download PDF");
		
		Button btnDisplayPieces = new Button(shlBrisbaneSymphonicBand, SWT.NONE);
		btnDisplayPieces.setBounds(10, 10, 90, 25);
		btnDisplayPieces.setText("Display Pieces");
		btnDisplayPieces.addMouseListener(new MouseAdapter() {
			public void mouseDown(MouseEvent arg0) {
				DBConnect conn = new DBConnect("jdbc:mysql://localhost:3306/capstone", "root", "secret");
				ResultSet rs = conn.Query("SELECT * FROM pieces");
				conn.DBClose();
			}
		});
		
		List list = new List(shlBrisbaneSymphonicBand, SWT.BORDER);
		list.setBounds(10, 41, 251, 114);
	}
}
