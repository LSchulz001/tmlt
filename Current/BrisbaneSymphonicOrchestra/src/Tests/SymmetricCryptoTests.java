package Tests;

import static org.junit.Assert.*;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.NoSuchPaddingException;

import org.junit.BeforeClass;
import org.junit.Test;

import DatabaseConn.Download;
import Security.SymmetricCrypto;

public class SymmetricCryptoTests {
	private static SymmetricCrypto key;
	
	@BeforeClass 
	public static void setup() throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException {
		key = new SymmetricCrypto("secret", 16, "AES");
	}
	
	@Test
	public void test() {
		assertTrue(key != null);
	}
}
