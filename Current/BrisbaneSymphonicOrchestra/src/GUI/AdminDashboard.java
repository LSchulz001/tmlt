package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.Window;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import DatabaseConn.Connect;

import java.awt.Font;
import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import java.awt.event.ActionListener;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.awt.event.ActionEvent;
import java.awt.Window.Type;

public class AdminDashboard {

	private JFrame frame;
	private Connect connection;

	/**
	 * Create the application.
	 */
	public AdminDashboard(Connect passedConnection) {
		connection = passedConnection;
		initialize();
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Path currentRelativePath = Paths.get("");
		String currentRelativePathString = currentRelativePath.toAbsolutePath().toString();
		
		String num = "";
		String num2 = "";
		frame = new JFrame();
		frame.setBounds(100, 100, 500, 199);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setTitle("Brisbane Symphonic Band");
		frame.setResizable(false);
		
		ImageIcon homeIcon = new ImageIcon(currentRelativePathString + "\\icons\\HomeIcon.png");
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(2, 2, 490, 166);
		frame.getContentPane().add(panel_2);
		panel_2.setBackground(new Color(13, 51, 92));
		panel_2.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(0, 0, 490, 35);
		panel.setBackground(new Color(13, 51, 92));
		panel_2.add(panel);
		
		JLabel lblWelcome = new JLabel("Welcome back " + connection.returnUser() + "!");
		lblWelcome.setBounds(10, 0, 180, 35);
		lblWelcome.setFont(new Font(Font.SERIF, Font.BOLD, 16));
		lblWelcome.setForeground(new Color(255,255,255));
		panel.add(lblWelcome);
		lblWelcome.setHorizontalAlignment(SwingConstants.LEFT);
		
		//The 'home' button that opens the main database window
		JButton btnHome = new JButton("Home", homeIcon);
		frame.getRootPane().setDefaultButton(btnHome);
		btnHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
				AdminHome adminHome = new AdminHome(connection);		
			}
		});
		btnHome.setBounds(391, 6, 89, 23);
		panel.add(btnHome);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(null, "SUMMARY", TitledBorder.LEADING, TitledBorder.TOP, new Font(Font.SERIF, Font.PLAIN, 10), new Color(255,255,255)));
		panel_3.setBounds(0, 34, 490, 132);
		panel_3.setBackground(new Color(13, 51, 92));
		panel_2.add(panel_3);
		panel_3.setLayout(null);
		
		JLabel lblBsbTitles = new JLabel("BSB Titles");
		lblBsbTitles.setBounds(20, 94, 70, 14);
		lblBsbTitles.setForeground(new Color(255,255,255));
		panel_3.add(lblBsbTitles);
		lblBsbTitles.setHorizontalAlignment(SwingConstants.CENTER);
		
		JLabel lblSwingTitles = new JLabel("Swing Titles");
		lblSwingTitles.setBounds(135, 94, 89, 14);
		lblSwingTitles.setForeground(new Color(255,255,255));
		panel_3.add(lblSwingTitles);
		lblSwingTitles.setHorizontalAlignment(SwingConstants.CENTER);
		
		JLabel lblBorrowedPieces = new JLabel("Borrowed Pieces");
		lblBorrowedPieces.setBounds(251, 94, 116, 14);
		lblBorrowedPieces.setForeground(new Color(255,255,255));
		panel_3.add(lblBorrowedPieces);
		lblBorrowedPieces.setHorizontalAlignment(SwingConstants.CENTER);
		
		JLabel lblUsers = new JLabel("Users\r\n");
		lblUsers.setBounds(408, 94, 48, 14);
		lblUsers.setForeground(new Color(255,255,255));
		panel_3.add(lblUsers);
		lblUsers.setHorizontalAlignment(SwingConstants.CENTER);
		
		num = num + connection.returnRowCount("pieces");
		JLabel lblBsbTitlesNumber = new JLabel(num);
		lblBsbTitlesNumber.setHorizontalAlignment(SwingConstants.CENTER);
		lblBsbTitlesNumber.setBounds(20, 36, 70, 47);
		lblBsbTitlesNumber.setForeground(new Color(255,255,255));
		panel_3.add(lblBsbTitlesNumber);
		
		JLabel lblSwingTitlesNumber = new JLabel("0");
		lblSwingTitlesNumber.setHorizontalAlignment(SwingConstants.CENTER);
		lblSwingTitlesNumber.setBounds(142, 36, 70, 47);
		lblSwingTitlesNumber.setForeground(new Color(255,255,255));
		panel_3.add(lblSwingTitlesNumber);
		
		JLabel lblBorrowedPiecesNumber = new JLabel(connection.returnLicenseCount() + "");
		lblBorrowedPiecesNumber.setHorizontalAlignment(SwingConstants.CENTER);
		lblBorrowedPiecesNumber.setBounds(275, 36, 70, 47);
		lblBorrowedPiecesNumber.setForeground(new Color(255,255,255));
		panel_3.add(lblBorrowedPiecesNumber);
		
		num2 = num2 + connection.returnRowCount("bandusers");
		JLabel lblUsersNumber = new JLabel(num2);
		lblUsersNumber.setHorizontalAlignment(SwingConstants.CENTER);
		lblUsersNumber.setBounds(396, 36, 70, 47);
		lblUsersNumber.setForeground(new Color(255,255,255));
		panel_3.add(lblUsersNumber);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(13, 51, 92));
		panel_1.setForeground(Color.BLACK);
		panel_1.setBounds(0, 0, 494, 170);
		frame.getContentPane().add(panel_1);
		
		//Show dialogue box to ensure user wants to exit
				frame.addWindowListener(new java.awt.event.WindowAdapter() {
				    @Override
				    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				        if (JOptionPane.showConfirmDialog(frame, 
				            "Are you sure you want to close this window?", "Close Window?", 
				            JOptionPane.YES_NO_OPTION,
				            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
				        	connection.closeConnection();
				            System.exit(0);
				        }
				    }
				});
				frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	}
	
	/**
	 * Makes the dashboard visible
	 */
	public void setVisible() {
		frame.setVisible(true);	
		frame.setLocationRelativeTo(null);
	}
}
