package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.codec.binary.Hex;

import DatabaseConn.Connect;
import DatabaseConn.Download;
import Security.ErrorBox;
import Security.Hash;
import Security.SymmetricCrypto;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.chrono.ChronoLocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.Color;
import java.awt.Dimension;

public class LoginPage {

	private JFrame frame;
	private JTextField txtUsername;
	private JPasswordField pswPassword;
	private Connect connection = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginPage window = new LoginPage();
				    window.frame.setLocationRelativeTo(null);
					window.frame.setVisible(true);
					window.frame.setResizable(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoginPage() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Path currentRelativePath = Paths.get("");
		String currentRelativePathString = currentRelativePath.toAbsolutePath().toString();
		
		//The frame for all swing objects on the Login Page
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(13, 51, 92));
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Brisbane Symphonic Band");
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{33, 50, 113, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{54, 20, 17, 34, 23, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		frame.getContentPane().setLayout(gridBagLayout);
		
		//Label indicating the Username field
		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setForeground(new Color(255, 255, 255));
		GridBagConstraints gbc_lblUsername = new GridBagConstraints();
		gbc_lblUsername.insets = new Insets(0, 0, 5, 5);
		gbc_lblUsername.gridx = 2;
		gbc_lblUsername.gridy = 1;
		frame.getContentPane().add(lblUsername, gbc_lblUsername);
		
		//Text field for the username
		txtUsername = new JTextField();
		GridBagConstraints gbc_txtUsername = new GridBagConstraints();
		gbc_txtUsername.insets = new Insets(0, 0, 5, 5);
		gbc_txtUsername.gridx = 4;
		gbc_txtUsername.gridy = 1;
		frame.getContentPane().add(txtUsername, gbc_txtUsername);
		txtUsername.setColumns(10);
		
		//Label indicating the Password field
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setForeground(new Color(255, 255, 255));
		GridBagConstraints gbc_lblPassword = new GridBagConstraints();
		gbc_lblPassword.insets = new Insets(0, 0, 5, 5);
		gbc_lblPassword.gridx = 2;
		gbc_lblPassword.gridy = 2;
		frame.getContentPane().add(lblPassword, gbc_lblPassword);
		
		//Password input field
		pswPassword = new JPasswordField();
		GridBagConstraints gbc_pswPassword = new GridBagConstraints();
		gbc_pswPassword.fill = GridBagConstraints.BOTH;
		gbc_pswPassword.insets = new Insets(0, 0, 5, 5);
		gbc_pswPassword.gridx = 4;
		gbc_pswPassword.gridy = 2;
		frame.getContentPane().add(pswPassword, gbc_pswPassword);
		
		//Login button
		JButton btnLogin = new JButton("Login");
		GridBagConstraints gbc_btnLogin = new GridBagConstraints();
		gbc_btnLogin.insets = new Insets(0, 0, 5, 0);
		gbc_btnLogin.anchor = GridBagConstraints.NORTH;
		gbc_btnLogin.gridwidth = 9;
		gbc_btnLogin.gridx = 0;
		gbc_btnLogin.gridy = 4;
		frame.getContentPane().add(btnLogin, gbc_btnLogin);
		frame.getRootPane().setDefaultButton(btnLogin);
		
		JLabel imgLabel = new JLabel();
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 0, 5);
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 7;
		imgLabel.setBounds(10, 24, 10, 10);
		BufferedImage img = null;
		
		//When the login button is pressed
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				String username = txtUsername.getText();
				char[] password = pswPassword.getPassword();
				Boolean passwordMatch = false;			
				String passwordStr = convertPassword(password);
				//Check if fields are empty
				// or check if the fields contain incompatible characters
				if(password.length == 0 || txtUsername.getText().isEmpty()) {
					JOptionPane pane = new JOptionPane();
					JDialog dialog = pane.createDialog(frame,"Error");
					pane.setMessageType(1);
					pane.setMessage("Please enter a username and password!");
					dialog.setSize(310, 120);
					dialog.setLocationRelativeTo(frame);
					dialog.setVisible(true);
				} else if (!username.matches("[a-zA-Z0-9]+")) {
					JOptionPane pane = new JOptionPane();
					JDialog dialog = pane.createDialog(frame,"Error");
					pane.setMessageType(1);
					pane.setMessage("Invalid characters entered");
					dialog.setSize(310, 120);
					dialog.setLocationRelativeTo(frame);
					dialog.setVisible(true);
				} else {
					try{
						connection = new Connect();
						//If the user is found in the database
						byte[] salt = connection.findUserSalt(username);
						if(salt != null) {
							//If the password hash matches the password in the database
							passwordMatch = connection.comparePassword(username, passwordStr, salt);
							if(passwordMatch) {
								//Log the user into a new page appropriate to their usertype
								if(connection.determineUserType(connection.returnUser()).equals("User")){
									String userFilePath = currentRelativePathString + "/" + username;
									File f = new File(userFilePath);
									if((!f.exists()) || (f.exists() && !f.isDirectory())) {
										f.mkdirs();
										
									} 
									
									try {
										SymmetricCrypto encryptionKey = new SymmetricCrypto(passwordStr, 16, "AES");
										//Overwrite current user details
										String hashedPassword = Hash.hashWithSalt(passwordStr, salt);
										String userDetailsPath = currentRelativePathString + "/" + username + "/userData.csv";
										PrintWriter writer = new PrintWriter(userDetailsPath, "UTF-8");
										Object[][] data = connection.getUserViewData();
										int rowCount = data.length;
										writer.println(rowCount);
										writer.println(username + "," + hashedPassword + "," + Hex.encodeHexString(salt));									
										String rowData = "";
										for(int row = 0; row < rowCount; row++) {
											for(int column = 0; column < data[row].length; column++) {
												rowData += data[row][column] + ",";
												
											}
											//Checksum to be implemented
											rowData += "Check Sum";
											writer.println(rowData);
											rowData = "";
										}
										writer.close();
										
										//Check if licenses are out of data and download if not
										HashMap<String, Boolean> results = licenseExpired(data);
										ArrayList<String> list = connection.getPDFTitles(username);
										for(String pdf : list) {
											String pdfPath = userFilePath + "/" + pdf;
											File pdfFile = new File(pdfPath);
											if(!pdfFile.exists() && !pdfFile.isDirectory() && results.get(pdf)) {
												
												String url = "http://localhost/Project/" + pdf;
												new Download(pdfPath, url);
												encryptionKey.encryptFile(pdfFile);
											}
										}
										
										//Check print log
										Object count[][] = connection.returnPrintCount(username);
										HashMap<Object, Object> countMap = new HashMap<Object, Object>();
										for(Object[] license : count) {
											
											countMap.put(license[0], license[1]);
											
										}
										String userPrintLog = currentRelativePathString + "/" + username + "/" + username + "PrintLog.log";
										File f2 = new File(userPrintLog);
										
										if(!f2.exists() ) { 
											//If the print log doesn't exist create a new one with details from DB 
											PrintWriter writer2 = new PrintWriter(userPrintLog, "UTF-8");
											for(int i = 0; i < count.length; i++) {
												writer2.write(count[i][0].toString() + "," + count[i][1].toString());
											}
											writer2.close();
											encryptionKey.encryptFile(f2);
										} else {
											//If print log does exist, check against DB
											encryptionKey.decryptFile(f2);
											
											BufferedReader br = new BufferedReader(new FileReader(userPrintLog));
											String line = null;
											String[] split = null;
											HashMap<Object, Object> temp = new HashMap<Object, Object>();
											while((line = br.readLine()) != null) {
												
												split = line.split(",");
												temp.put(split[0], split[1]);
											}
											br.close();
											//test
											boolean dataMismatch = false;
											for(Object license : temp.keySet()) {
												Object tempCompare = countMap.get(license);
												
												if(Integer.parseInt((String) temp.get(license)) < (Integer)tempCompare ) {
													//If the DB version is greater than local version
													dataMismatch = true;
												} else if(Integer.parseInt((String)temp.get(license)) > (Integer)countMap.get(license)) {
													//If local version greater than DB version
													
													connection.updatePrintCount(license, temp.get(license));
													countMap.replace(license, temp.get(license));
												}
											}
											if(dataMismatch) {
												//Write changes to local disk
												PrintWriter writer3 = new PrintWriter(userPrintLog, "UTF-8");
												for(Object license : countMap.keySet()) {
													writer3.write(license.toString() + "," + countMap.get(license).toString());
												}
												writer3.close();
											}
											encryptionKey.encryptFile(f2);
										}
										
										File f3 = new File(userDetailsPath);
										encryptionKey.encryptFile(f3);
										//encryptionKey.decryptFile(f2);
										UserHome userHome = new UserHome(connection, encryptionKey, username, count, passwordStr);
									} catch ( NoSuchAlgorithmException
											| NoSuchPaddingException | IOException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								} else if(connection.determineUserType(connection.returnUser()).equals("Admin")) {
									AdminDashboard adminDashboard = new AdminDashboard(connection);
									adminDashboard.setVisible();
								}
								
								frame.dispose();
							} else {
								connection.closeConnection();
								ErrorBox.createErrorBox(frame, "Username or Password is incorrect", "Error", 0);
							}
						} else {
							connection.closeConnection();
							ErrorBox.createErrorBox(frame, "Username or Password is incorrect", "Error", 0);
						}
					}
					catch(SQLException e){
						
						String userFilePath = currentRelativePathString + "/" + username + "/userData.csv";
						String userLogFilePath = currentRelativePathString + "/" + username + "/" + username + "printLog.log";
						File f = new File(userFilePath);
						File f2 = new File(userLogFilePath);
						if(!f.exists() || !f2.exists()) {
							ErrorBox.createErrorBox(frame, "User data not on disk, "
									+ "please connect\nto the internet for first-time login", "Connection Error", 0);
							e.printStackTrace();
						} else {
							try {
								SymmetricCrypto encryptionKey = new SymmetricCrypto(passwordStr, 16, "AES");
								encryptionKey.decryptFile(f);
								BufferedReader br = new BufferedReader(new FileReader(f));
								String line; 
								br.readLine();
								
								line = br.readLine();
								String[] userDetails = line.split(",");
								String hashedPassword = Hash.hashWithSalt(passwordStr, Hash.hexStringToByteArray(userDetails[2]));
								br.close();
								encryptionKey.encryptFile(f);
								if(hashedPassword.equals(userDetails[1])) {
									ArrayList<Object[]> newData = new ArrayList<Object[]>();
									encryptionKey.decryptFile(f2);
									br = new BufferedReader(new FileReader(f2));
									
									System.out.println("Reading");
									while((line = br.readLine()) != null) {
										System.out.println(line);
										newData.add(line.split(","));
									}
									
									br.close();
									encryptionKey.encryptFile(f2);
									Object[][]licenseData = new Object[newData.size()][2];
									int i = 0;
									for(Object[] row : newData) {
									
										licenseData[i][0] = Integer.parseInt((String)row[0]);
										licenseData[i][1] = Integer.parseInt((String)row[1]);
										i++;
									}
									UserHome userHome = new UserHome(encryptionKey, username, licenseData, passwordStr);
									
									
								} else {
									ErrorBox.createErrorBox(frame, "Incorrect login for that user, "
											+ "please try again", "Invalid login", 0);
									encryptionKey.encryptFile(f);
								}
							} catch ( NoSuchAlgorithmException
									| NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | IOException e1) {
								e1.printStackTrace();
							}
						}
					}
				}
			}
		});
		
		//Show dialogue box to ensure user wants to exit
				frame.addWindowListener(new java.awt.event.WindowAdapter() {
				    @Override
				    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				        if (JOptionPane.showConfirmDialog(frame, 
				            "Are you sure you want to close this window?", "Close Window?", 
				            JOptionPane.YES_NO_OPTION,
				            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
				        	if(connection != null) {
				        		connection.closeConnection();
				        	}
				            System.exit(0);
				        }
				    }
				});
				frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	    frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	
	/**
	 * Concatenates the characters of the given array into a string and returns the string
	 * @param characters - The array of letters to be converted
	 * @return the complete string
	 */
	private static String convertPassword(char[] characters) {
		String password = "";
		for(int i = 0; i < characters.length; i++) {
			password += characters[i];
		}
		return password;
	}
	
	/**
	 * For each pdf, checks if it is out of date 
	 * @param rowData - The object[][] of the parts and their informaition
	 * @return Hashmap of pdf name and boolean corresponding to its expiration (false if expired)
	 */
	private static HashMap<String, Boolean> licenseExpired(Object[][] rowData) {
		HashMap<String, Boolean> licenseExpired = new HashMap<String, Boolean>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		for(Object row[] : rowData) {
			Date date1 = null;
			try {
				date1 = sdf.parse((String) row[3]);
				
			} catch (ParseException e) {
				e.printStackTrace();
			}
			LocalDate date2 = LocalDate.now();
			Date date3 = Date.from(date2.atStartOfDay(ZoneId.systemDefault()).toInstant());
			if(date3.after(date1)) {
				licenseExpired.put((String) row[5], false);
			} else {
				licenseExpired.put((String) row[5], true);
			}
		}
		return licenseExpired;
	}
}
