package DatabaseConn;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;

import org.apache.pdfbox.pdmodel.PDDocument; 
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer; 
import org.apache.pdfbox.tools.imageio.ImageIOUtil;

public class Download {
	
	/**
	 * Constructor to download a file
	 * @param path - path to the location for the file to be created
	 * @param url - the URL of the server to download from
	 */
	public Download(String path, String url) {
		createPDF(path);
		try {
			ReadableByteChannel in=Channels.newChannel(new URL(url).openStream());
			FileChannel out = new FileOutputStream(path).getChannel();
			out.transferFrom(in, 0, Long.MAX_VALUE);
			out.close();
		} catch(IOException ex){
		    
		    ex.printStackTrace();
		}
		try (final PDDocument document = PDDocument.load(new File(path))){
            PDFRenderer pdfRenderer = new PDFRenderer(document);
            for (int page = 0; page < 1; ++page)
            {
                BufferedImage bim = pdfRenderer.renderImageWithDPI(page, 300, ImageType.RGB);
                String fileName = path + "image-" + page + ".png";
                ImageIOUtil.writeImage(bim, fileName, 300);
            }
            document.close();
        } catch (IOException e){
            System.err.println("Exception while trying to create pdf document - " + e);
        }
	}
	
	/**
	 * Creates a blank pdf
	 * @param path - path to the creation of the file
	 */
	public void createPDF(String path) {
		//Creating PDF document object 
	    PDDocument document = new PDDocument();     
	     
	    //Add an empty page to it 
	    document.addPage(new PDPage()); 
	      
	    try {
	    	//Saving the document 
			document.save(path);
			//Closing the document  
		    document.close(); 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	        
	}
}

