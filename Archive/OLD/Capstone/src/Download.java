import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import org.apache.pdfbox.pdmodel.PDDocument; 
import org.apache.pdfbox.pdmodel.PDPage; 

public class Download {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Download();

	}
	
	/**
	 * Constructor to download a file
	 */
	public Download() {
		createPDF();
		try {
			ReadableByteChannel in=Channels.newChannel(new URL("http://localhost/Capstone/tmlt/w1_clab.pdf").openStream());
			FileChannel out = new FileOutputStream("C:/Capstone/tmlt/BlankPDF.pdf").getChannel();
			out.transferFrom(in, 0, Long.MAX_VALUE);
			out.close();
		} catch(IOException ex){
		    System.out.println("Error...!!");
		    ex.printStackTrace();
		}
	}
	
	/**
	 * Creates a blank pdf
	 */
	public void createPDF() {
		//Creating PDF document object 
	    PDDocument document = new PDDocument();     
	     
	    //Add an empty page to it 
	    document.addPage(new PDPage()); 
	      
	    try {
	    	//Saving the document 
			document.save("C:/Capstone/tmlt/BlankPdf.pdf");
			//Closing the document  
		    document.close(); 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	      System.out.println("PDF created");  
	}
}
