package Tests;
import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import DatabaseConn.Connect;

/*
 * These tests assume a development database with certain variables
 */
public class ConnectTests {
	private static Connect connect = null;
	private static JFrame frame;
	
	@BeforeClass
	public static void databaseConnection() throws SQLException {
		connect = new Connect();
		frame = new JFrame();
	}
	
	@Test
	public void testConnection() throws SQLException {
		assertTrue(connect != null);
	}
	
	@Test
	public void testReturnData() throws SQLException {
		Object[][] data = connect.returnDBData("pieces");
		assertEquals(Integer.parseInt((String)data[0][0]), 1);
	}
	
	@Test
	public void testFindUserSalt() throws SQLException{
		String username = "lschu7";
		byte[] salt = connect.findUserSalt(username);
		assertTrue(salt != null);
	}
	
	@Test
	public void testComparePassword() throws SQLException {
		String username = "lschu7";
		String password = "password2";
		byte[] salt = connect.findUserSalt(username);
		boolean success = connect.comparePassword(username, password, salt);
		assertTrue(salt != null);
	}
	
	@Test
	public void testUserType() throws SQLException {
		String username = "lschu7";
		String type = connect.determineUserType(username);
		assertTrue(type.equals("User"));
	}
	
	@Test
	public void searchingTerm() throws SQLException {
		Object[][] data = connect.returnFilteredData("pieces", "Intro", "Title");
		assertEquals(Integer.parseInt((String) data[0][0]), 1);
	}
	
	@Test
	public void testAdd() throws SQLException {
		List<Object> entries = new ArrayList<Object>();
		entries.add(700);
		for(int i = 0; i < 8; i++) {
			entries.add(i);
		}
		entries.add(0);
		entries.add(0);
		for(int i = 0; i < 4; i++) {
			entries.add(i);
		}
		connect.addNewEntry(entries, "pieces", frame);
		Object[][] data = connect.returnDBData("pieces");
		assertEquals(Integer.parseInt((String)data[data.length - 1][0]), 700);
	}
	
	@Test
	public void testUpdate() throws SQLException {
		List<Object> entries = new ArrayList<Object>();
		entries.add(new String[] {"700"});
		for(int i = 0; i < 8; i++) {
			entries.add("" + i);
		}
		entries.add(1);
		entries.add(2);
		for(int i = 0; i < 4; i++) {
			entries.add("" + i);
		}
		boolean success = connect.updateEntry(entries, "pieces", frame);
		assertTrue(success);
	}
	
	@Test 
	public void testGetLastID() throws SQLException {
		int id = connect.getLastID("pieces");
		assertEquals(id, 700);
	}
	
	@Test
	public void getPrintCount() throws SQLException {
		Object[][] data = connect.returnPrintCount("lschu7");
		assertEquals(data[0][1], 4);
	}
	
	@Test 
	public void updatePrintCount() throws SQLException {
		connect.updatePrintCount(1, 1);
		Object[][] data = connect.returnPrintCount("demoUser");
		assertEquals(1, data[0][1]);
	}
	
	@Test 
	public void getPDFTitles() throws SQLException {
		List<String> data = connect.getPDFTitles("lschu7");
		assertTrue(data.get(0).equals("example2.pdf"));
	}
	
	@Test
	public void getLicenseCount() throws SQLException {
		int count = connect.returnLicenseCount();
		assertEquals(3, count);
	}
	
	@Test
	public void testDelete() throws SQLException {
		boolean success = connect.removeItem("pieces", new String[] {"700"});
		assertTrue(success);
	}
	
	@AfterClass
	public static void closeConnection() {
		connect.closeConnection();
	}
}
