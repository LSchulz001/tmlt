package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import org.icepdf.ri.common.ComponentKeyBinding;
import org.icepdf.ri.common.SwingController;
import org.icepdf.ri.common.SwingViewBuilder;

import DatabaseConn.Connect;
import Security.ErrorBox;
import Security.SymmetricCrypto;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Image;

import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import java.awt.Component;
import javax.swing.ScrollPaneConstants;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;

public class UserHome {

	private JFrame frame;
	private DefaultTableModel modelUserView;
	private static Connect connection;
	private SymmetricCrypto encryptionKey;
	private String userName;
	private Object licenseData[][];
	private Object userData[][];
	private String userPass;

	/**
	 * Create the application. This constructor won't be needed when development is finished
	 */
	public UserHome() {
		initialize();
	}
	
	/**
	 * Create the application.
	 */
	public UserHome(Connect connect, SymmetricCrypto key, String username, Object[][] licensedata, String secret) {
		connection = connect;
		encryptionKey = key;
		userName = username;
		licenseData = licensedata;
		userPass = secret;
		initialize();
	}
	
	/**
	 * Create the application in offline mode.
	 */
	public UserHome(SymmetricCrypto key, String username, Object[][] licensedata, String secret) {
		encryptionKey = key;
		userName = username;
		licenseData = licensedata;
		userPass = secret;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Brisbane Symphonic Band");
		frame.getContentPane().setBackground(new Color(13, 51, 92));
		frame.setBounds(100, 100, 1280, 720);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);

		Path currentRelativePath = Paths.get("");
		String currentRelativePathString = currentRelativePath.toAbsolutePath().toString();
		String userDir = currentRelativePathString + "/" + userName;

		ImageIcon logoutImage = new ImageIcon(
				currentRelativePathString + "\\icons\\LogoutIcon.png");
		ImageIcon viewImage = new ImageIcon(
				currentRelativePathString + "\\icons\\ViewIcon.png");
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(0, 0, 1280, 653);
		panel.setBackground(new Color(13, 51, 92));
		frame.getContentPane().add(panel);

		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.WHITE);
		panel_2.setLayout(null);
		panel_2.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "PREVIEW", TitledBorder.LEADING,
				TitledBorder.TOP, null, new Color(255, 255, 255)));
		panel_2.setBackground(new Color(13, 51, 92));
		panel_2.setBounds(649, 43, 600, 550);

		panel.add(panel_2);

		JPanel panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(10, 51, 600, 540);
		panel_1.setBackground(new Color(13, 51, 92));
		panel.add(panel_1);
		
		JTable tableUserView = new JTable();
		//On mouse click of table row, display the preview of the PDF
		tableUserView.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent arg0) {
							int row = tableUserView.getSelectedRow();
							if(row != -1) {
								
								String title = (String) userData[row][5];
								String[] split = title.split("\\.");
								
								String pdfImage = split[0] + ".pdfimage-0.png";
						
								BufferedImage img = null;
								JLabel imgLabel = new JLabel();
								panel_2.removeAll();
								imgLabel.setBounds(10, 24, 580, 515);
								try {
								    img = ImageIO.read(new File(userDir + "/" + pdfImage));
								} catch (IOException e) {
								    e.printStackTrace();
								}
								Image dimg = img.getScaledInstance(imgLabel.getWidth(), imgLabel.getHeight(),
								        Image.SCALE_SMOOTH);
								imgLabel.setIcon(new ImageIcon(dimg));
								
								imgLabel.setVisible(false);
								panel_2.add(imgLabel);
								imgLabel.setVisible(true);
							}
						}
					});
		
		tableUserView.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		tableUserView.setFillsViewportHeight(true);
		tableUserView.setAutoCreateRowSorter(true);

		JScrollPane scrollPane_1 = new JScrollPane(tableUserView);
		scrollPane_1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane_1.setBounds(10, 10, 580, 525);
		panel_1.add(scrollPane_1);

		JPanel panel_3 = new JPanel();
		panel_3.setBounds(10, 604, 1217, 38);
		panel_3.setBackground(new Color(13, 51, 92));
		panel.add(panel_3);
		
		JButton btnLogout = new JButton("Logout", logoutImage);
		btnLogout.setFont(new Font(Font.SERIF, Font.BOLD, 17));
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane.showConfirmDialog (null, "Are you sure?","question",dialogButton);
				if(dialogResult == JOptionPane.YES_OPTION){
					frame.dispose();
					connection.closeConnection();
					LoginPage login = new LoginPage();
			}
			}
		});
		btnLogout.setBounds(1154, 0, 119, 40);
		panel.add(btnLogout);
		frame.setVisible(true);
		
		//Start the viewer application
		JButton btnViewpdf = new JButton("View", viewImage);
		btnViewpdf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(tableUserView.getSelectedRow() != -1) {
					// Create a JFrame to display the panel in
					//PDF to view
					int row = tableUserView.getSelectedRow();

					String title = (String) userData[row][5];

					String filePath = userDir + "/" + title;

					// build a controller
					SwingController controller = new SwingController();

					// Build a SwingViewFactory configured with the controller
					SwingViewBuilder factory = new SwingViewBuilder(controller);

					// Give ICE our own variables
					controller.setUsername(userName);
					controller.setCountData(licenseData);
					controller.setSecret(userPass);
					controller.setLength(16);
					controller.setPrintJob((String)userData[row][6]);

					// Use the factory to build a JPanel that is pre-configured
					// with a complete, active Viewer UI.
					JPanel viewerComponentPanel = factory.buildViewerPanel();

					// add copy keyboard command
					ComponentKeyBinding.install(controller, viewerComponentPanel);

					// add interactive mouse link annotation support via
					// callback
					controller.getDocumentViewController().setAnnotationCallback(
							new org.icepdf.ri.common.MyAnnotationCallback(controller.getDocumentViewController()));

					File f = new File(filePath);
					if (f.exists()) {
						try {
							encryptionKey.decryptFile(f);
							// Open a PDF document to view
							controller.openDocument(filePath);
							encryptionKey.encryptFile(f);
						} catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException
								| IOException e) {
							e.printStackTrace();
						}

						JFrame window = new JFrame("Using the Viewer Component");
						window.getContentPane().add(viewerComponentPanel);
						window.pack();
						window.setVisible(true);
					} else {
						ErrorBox.createErrorBox(frame, "File does not exist", "Error", 0);
					}					
				} else{
					ErrorBox.createErrorBox(frame, "Please select a part", "Error", 1);
				}
			}
		});
		panel_3.add(btnViewpdf);

		String[] userViewNames = {"Title", "Instrument", "Borrow Date", "Due Date", "Band Name"};
		String userDetailsPath = currentRelativePathString + "/" + userName + "/" + "userData.csv";
		File f = new File(userDetailsPath);
		userData = null;
		try {
			encryptionKey.decryptFile(f);
			BufferedReader br = new BufferedReader(new FileReader(f));
			String[] commaSplit;
			//Get amount of rows
			String line = br.readLine();
			HashMap<Integer, Object[]> data = new HashMap<Integer, Object[]>();
			
			
			int i = 0;
			//Skip user data
			br.readLine();
			//Read in licensing data
			while ((line = br.readLine()) != null) {
				commaSplit = line.split(",");
				if(!licenseExpired(commaSplit[3])) {
					Object[] temp = new Object[8];
					for(int j = 0; j < commaSplit.length; j++) {
						temp[j] = commaSplit[j];						
					}
					
					data.put(i, temp);
					i++;
				}
				
			}
			encryptionKey.encryptFile(f);
			
			userData = new Object[data.size()][8];
			for(int j = 0; j < data.size(); j++) {
				userData[j] = data.get(j);
			}
		} catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException | IOException e1) {
			e1.printStackTrace();
		}
		tableUserView.setModel(new DefaultTableModel(userData, userViewNames) {
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		});
		
		modelUserView = (DefaultTableModel) tableUserView.getModel();
		
		//Show dialogue box to ensure user wants to exit
				frame.addWindowListener(new java.awt.event.WindowAdapter() {
				    @Override
				    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				        if (JOptionPane.showConfirmDialog(frame, 
				            "Are you sure you want to close this window?", "Close Window?", 
				            JOptionPane.YES_NO_OPTION,
				            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
				        	if(connection != null) {
				        		connection.closeConnection();
				        	}
				            System.exit(0);
				        }
				    }
				});
				frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	}
	
	/**
	 * Returns true if the submitted date is past the current date
	 * @param endDate - The end date of the license
	 * @return the state of license expiration
	 */
	private static boolean licenseExpired(String endDate) {
		boolean licenseExpired = false;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Date date1 = null;
		try {
			date1 = sdf.parse(endDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		LocalDate date2 = LocalDate.now();
		Date date3 = Date.from(date2.atStartOfDay(ZoneId.systemDefault()).toInstant());
		if(date3.after(date1)) {
			licenseExpired = true;
		}
		
		return licenseExpired;
	}
}
