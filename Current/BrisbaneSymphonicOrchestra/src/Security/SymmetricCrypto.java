package Security;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.JOptionPane;

public class SymmetricCrypto {
	private SecretKeySpec secretKey;
	private Cipher cipher;
	
	/**
	 * Creates an object to hold the information associated with an encrpytion key
	 * @param secret - secret for the key
	 * @param length - length of the key
	 * @param algorithm - algorithm to encrypt the secret with
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 */
	public SymmetricCrypto(String secret, int length, String algorithm)
			throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException {
		byte[] key = new byte[length];
		key = fixSecret(secret, length);
		this.secretKey = new SecretKeySpec(key, algorithm);
		this.cipher = Cipher.getInstance(algorithm);
	}
	
	/**
	 * Fixes the secret to be the length specified
	 * @param s - the secret string 
	 * @param length - length to be fixed to
	 * @return - a byte array of the secret to the fixed length
	 * @throws UnsupportedEncodingException
	 */
	private byte[] fixSecret(String s, int length) throws UnsupportedEncodingException {
		if (s.length() < length) {
			int missingLength = length - s.length();
			for (int i = 0; i < missingLength; i++) {
				s += " ";
			}
		}
		return s.substring(0, length).getBytes("UTF-8");
	}

	/**
	 * Encrypts the specified file with the key
	 * @param f - the file to be encrypted
	 * @throws InvalidKeyException Thrown when the key is invalid
	 * @throws IOException Thrown when the input-output process is interrupted
	 * @throws IllegalBlockSizeException Thrown when the block size is invalid
	 * @throws BadPaddingException Thrown when the padding is invalid
	 */
	public void encryptFile(File f)
			throws InvalidKeyException, IOException, IllegalBlockSizeException, BadPaddingException {
		
		this.cipher.init(Cipher.ENCRYPT_MODE, this.secretKey);
		this.writeToFile(f);
	}
	
	/**
	 * Decrypts the specified file
	 * @param f - the file to be decrypted
	 * @throws InvalidKeyException Thrown when the key is invalid
	 * @throws IOException Thrown when the input-output process is interrupted
	 * @throws IllegalBlockSizeException Thrown when the block size is invalid
	 * @throws BadPaddingException Thrown when the padding is invalid
	 */
	public void decryptFile(File f)
			throws InvalidKeyException, IOException, IllegalBlockSizeException, BadPaddingException {
		
		this.cipher.init(Cipher.DECRYPT_MODE, this.secretKey);
		this.writeToFile(f);
	}

	/**
	 * Writes the encryption or decryption to the file
	 * @param f - file to be encrypted or decrypted
	 * @throws IOException Thrown when the input-output process is interrupted
	 * @throws IllegalBlockSizeException Thrown when the block size is invalid
	 * @throws BadPaddingException Thrown when the padding is invalid
	 */
	public void writeToFile(File f) throws IOException, IllegalBlockSizeException, BadPaddingException {
		FileInputStream in = new FileInputStream(f);
		byte[] input = new byte[(int) f.length()];
		in.read(input);

		FileOutputStream out = new FileOutputStream(f);
		byte[] output = this.cipher.doFinal(input);
		out.write(output);

		out.flush();
		out.close();
		in.close();
	}
}
