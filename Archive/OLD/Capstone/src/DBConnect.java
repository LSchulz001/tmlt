import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;


public class DBConnect {
	private Connection conn = null;
	
	/**
	 * Constructor for Database connection
	 * "jdbc:mysql://localhost:3306/capstone"
	 * "root"
	 * "secret"
	 */
	public DBConnect(String url, String userName, String password) {
		// TODO Auto-generated constructor stub
				
				try {
					conn = DriverManager.getConnection(url, userName, password);
					Statement stmt = (Statement)conn.createStatement();
					ResultSet rs = stmt.executeQuery("Select * FROM pieces");
					int id = 0;
					String name = null;
					String pdf = null;
					//Display all table items
					while (rs.next())
					{
					   id = rs.getInt("pieceID");
					   name = rs.getString("pieceName");
					   pdf = rs.getString("piecePDF");
					   System.out.println(id + " " + name + " " + pdf);
					}
					
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
	}
	
	/**
	 * Close the connection to a database
	 */
	public void DBClose() {
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ResultSet Query (String query) {
		Statement stmt;
		ResultSet rs = null;
		try {
			stmt = (Statement)conn.createStatement();
			rs = stmt.executeQuery(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return rs;
	}

}
