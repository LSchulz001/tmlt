package Security;

import java.awt.EventQueue;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import GUI.LoginPage;

public class Hash {

	/**
	 * Hashes the given string passwordToHash using the given salt and returns the hash 
	 * @param passwordToHash - the plaintext password string
	 * @param salt - a randomly generated salt in a byte array
	 * @return The hash of the password
	 */
	public static String hashWithSalt(String passwordToHash, byte[] salt) {
		String hashedPassword = null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            //Add password bytes to digest
            md.update(salt);
            //Get the hash's bytes
            byte[] bytes = md.digest(passwordToHash.getBytes());
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            hashedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return hashedPassword;
	}
	
	/**
	 * Generates a secure random salt and returns it as a byte array
	 * @return a secure, randomly generated salt in a byte array
	 * @throws NoSuchAlgorithmException thrown when the algorithm doesn't exist
	 * @throws NoSuchProviderException thrown when the provider doesn't exist
	 */	
	public static byte[] getSalt()
    {
        //Always use a SecureRandom generator
        SecureRandom sr = null;
		try {
			sr = SecureRandom.getInstance("SHA1PRNG", "SUN");
		} catch (NoSuchAlgorithmException | NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        //Create array for salt
        byte[] salt = new byte[16];
        //Get a random salt
        sr.nextBytes(salt);
        //return salt
        return salt;
    }
	
	/**
	 * Converts a hexadecimal string into an equivalent byte array and returns it
	 * @param s - the hexadecimal string to be converted
	 * @return a byte array of equivalent numerical value
	 */
	public static byte[] hexStringToByteArray(String s) {
	    byte[] b = new byte[s.length() / 2];
	    for (int i = 0; i < b.length; i++) {
	      int index = i * 2;
	      b[i] = (byte) Integer.parseInt(s.substring(index, index + 2), 16);
	    }
	    return b;
	}
	
	/**
	 * Concatenates the characters of the given array into a string and returns the string
	 * @param characters - The array of letters to be converted
	 * @return the complete string
	 */
	public static String convertPassword(char[] characters) {
		String password = "";
		for(int i = 0; i < characters.length; i++) {
			password += characters[i];
		}
		return password;
	}
}
